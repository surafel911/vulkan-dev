#pragma once

#include <cstdlib>
#include <iostream>
#include <string_view>

#include "src/base/tiny_obj_loader.h"

namespace vulkanexamples::base {
    class Mesh {
        public:
            Mesh() = default;
            ~Mesh() = default;

            static int LoadObj(std::string_view path, Mesh& mesh);

            inline const tinyobj::attrib_t& GetAttributes() { return this->_attributes; }
            inline const std::vector<tinyobj::shape_t>& GetShapes() { return this->_shapes; }
            inline const std::vector<tinyobj::material_t>& GetMaterials() { return this->_materials; }

        private:
            tinyobj::attrib_t _attributes {};
            std::vector<tinyobj::shape_t> _shapes {};
            std::vector<tinyobj::material_t> _materials {};

            Mesh(const tinyobj::attrib_t& attributes,
                 const std::vector<tinyobj::shape_t>& shapes,
                 const std::vector<tinyobj::material_t>& materials
                ) : _attributes{attributes}, _shapes{shapes}, _materials{materials}
            { }
    };
}