#pragma once

#include <vector>
#include <cstdint>
#include <optional>
#include <string_view>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.h>

#define VK_CHECK(VK_FUNC) \
if (VkResult result = VK_FUNC; result != VK_SUCCESS) { \
    std::cerr << "[Vulkan] [ERROR] VK function `" << #VK_FUNC <<"' has failed: " << -result << std::endl; \
    return result; \
}

#define VK_CHECK_R(VK_FUNC, VK_RES) \
if (VkResult result = static_cast<VkResult>(VK_FUNC); result != VK_SUCCESS) { \
    std::cerr << "[Vulkan] [ERROR] VK function `" << #VK_FUNC <<"' has failed: " << -VK_RES << std::endl; \
    return VK_RES; \
}

#define VK_GENERATE_MAIN(T) \
int main(int argc, char* argv[]) { \
    T app {}; \
    app.Run(argc, argv); \
 \
    return EXIT_SUCCESS; \
}

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

namespace vulkanexamples::base {
    struct VulkanSettings {
        std::string_view title {"Vulkan Example"};
        SDL_Rect window_size {SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 800};
        bool enable_validation {true};
    };

    class VulkanExampleBase {
        public:
            virtual void Run(const int argc, char* const argv[]);

        protected:
            static constexpr int MAX_FRAMES_IN_FLIGHT {2};

            struct QueueFamilyIndices {
                std::optional<std::uint32_t> graphics_queue_family {};
                std::optional<std::uint32_t> present_queue_family {};

                inline bool IsComplete() {
                    return  graphics_queue_family.has_value()
                            && present_queue_family.has_value();
                }
            };

            struct SwapChainDetails {
                VkSurfaceCapabilitiesKHR capabilities {};
                VkSurfaceFormatKHR format {};
                VkPresentModeKHR present_mode {};
                VkExtent2D extent {};
                std::uint32_t image_count {};
            };

            struct SwapChainSupportDetails {
                VkSurfaceCapabilitiesKHR capabilities {};
                std::vector<VkSurfaceFormatKHR> formats {};
                std::vector<VkPresentModeKHR> present_modes {};
            };

            VulkanSettings _settings {};

            std::vector<const char*> _enabled_instance_extensions;
            std::vector<const char*> _enabled_device_extensions;
            std::vector<const char*> _validation_layers;

            VkInstance _instance {};

            VkDebugUtilsMessengerEXT _debug_messenger {};
            PFN_vkCreateDebugUtilsMessengerEXT _vkCreateDebugUtilsMessengerEXT {};
            PFN_vkDestroyDebugUtilsMessengerEXT _vkDestroyDebugUtilsMessengerEXT {};

            VkSurfaceKHR _surface {};

            VkPhysicalDevice _physical_device {};
            VkPhysicalDeviceFeatures _physical_device_features {};
            VkPhysicalDeviceProperties _physical_device_properties {};

            QueueFamilyIndices _queue_family_indices {};

            VkDevice _device {};

            VkQueue _graphics_queue {};
            VkQueue _present_queue {};

            VkSwapchainKHR _swap_chain {};
            SwapChainDetails _swap_chain_details {};

            std::vector<VkImage> _swap_chain_images {};
            std::vector<VkImageView> _swap_chain_image_views {};

            std::vector<VkFramebuffer> _swap_chain_framebuffers {};

            VkBool32 _framebuffer_resized {};

            double _delta_time {};

            VulkanExampleBase() = default;

            VulkanExampleBase(const VulkanSettings& settings)
                :   _settings(settings)
            { }

            virtual ~VulkanExampleBase() { };

            VulkanExampleBase(VulkanExampleBase &&) = delete;
            VulkanExampleBase &operator=(VulkanExampleBase &&) = delete;
            VulkanExampleBase(const VulkanExampleBase&) = delete;
            VulkanExampleBase& operator=(const VulkanExampleBase&) = delete;

            virtual VkResult setup() = 0;
            virtual void terminate() = 0;
            virtual VkResult renderFrame() = 0;
            virtual SDL_bool handleEvent(const SDL_Event& event);
            int readFile(std::string_view path, std::vector<std::byte>& buffer);

            void waitEvent();

            VkResult recreateSwapChain();

            VkResult createShaderModule(const std::vector<std::byte>& code, VkShaderModule& shader_module);

        private:
            SDL_Window* _window {};

            bool _quit {};

            int initSDL();
            void quitSDL();


            VkResult initVulkan();
            void quitVulkan();

            VkResult createInstance(VkDebugUtilsMessengerCreateInfoEXT* debug_messenger_create_info);
            void destroyInstance();

            VkResult createDebugMessenger(VkDebugUtilsMessengerCreateInfoEXT* debug_messenger_create_info);
            void destroyDebugMessenger();

            VkResult createSurface();
            void destroySurface();

            VkResult pickPhysicalDevice();
            VkBool32 isPhysicalDeviceSuitable(const VkPhysicalDevice& physical_device);
            void findQueueFamilies(const VkPhysicalDevice& physical_device);
            VkResult queryDeviceExtensions(const VkPhysicalDevice& physical_device, std::vector<VkExtensionProperties>& device_extensions);
            bool checkDeviceExtensionSupport(std::vector<VkExtensionProperties>& device_extensions);
            VkResult querySwapChainDetails(const VkPhysicalDevice& physical_device, SwapChainSupportDetails& swap_chain_details);
            bool isSwapChainSupported(const SwapChainSupportDetails& swap_chain_details);

            VkResult createLogicalDevice();
            void destroyLogicalDevice();
            void getGraphicsQueue();
            void getPresentQueue();

            VkResult createSwapChain();
            void destroySwapChain();
            VkSurfaceFormatKHR chooseSwapChainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats);
            VkPresentModeKHR chooseSwapChainPresentMode(const std::vector<VkPresentModeKHR>& present_modes);
            VkExtent2D chooseSwapChainExtent(const VkSurfaceCapabilitiesKHR& surface_capabilities);

            VkResult createSwapChainImages();
            void destroySwapChainImages();

            VkResult createSwapChainImageViews();
            void destroySwapChainImageViews();
    };
}
