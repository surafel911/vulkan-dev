#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace vulkanexamples::base {
    /* An abstract camera class that processes input and
     * calculates the corresponding Euler Angles, Vectors
     * and Matrices for use in OpenGL
     */
    class Camera {
        public:
            /* Defines several possible options for camera
             * movement. Used as abstraction to stay away
             * from window-system specific input methods.
             */
            enum class Movement {
                FORWARD,
                BACKWARD,
                LEFT,
                RIGHT,
                UP,
                DOWN
            };
            // Camera Attributes
            glm::vec3 Position;
            glm::vec3 Front;
            glm::vec3 Up;
            glm::vec3 Right;
            glm::vec3 WorldUp;

            // Euler Angles
            float Yaw;
            float Pitch;

            // Camera options
            float MovementSpeed;
            float MouseSensitivity;
            float Zoom;

            // Constructor with vectors
            Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH)
                : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM) {
                Position = position;
                WorldUp = up;
                Yaw = yaw;
                Pitch = pitch;

                this->updateCameraVectors();
            }

            // Constructor with scalar values
            Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch)
                : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM) {
                Position = glm::vec3(posX, posY, posZ);
                WorldUp = glm::vec3(upX, upY, upZ);
                Yaw = yaw;
                Pitch = pitch;

                this->updateCameraVectors();
            }

            /* Returns the view matrix calculated using Euler Angles
             * and the LookAt Matrix
             */
            glm::mat4 GetViewMatrix();

            /* Processes input received from any keyboard-like input
             * system. Accepts input parameter in the form of camera
             * defined ENUM (to abstract it from windowing systems)
             */
            void ProcessKeyboard(Movement direction, float deltaTime);

            /* Processes input received from a mouse input system.
             * Expects the offset value in both the x and y direction.
             */
            void ProcessMouseMovement(float xoffset, float yoffset, bool constrainPitch = false);

            /* Processes input received from a mouse scroll-wheel
             * event. Only requires input on the vertical wheel-axis.
             */
            void ProcessMouseScroll(float yoffset);

        private:
            // Default camera values
            static constexpr float YAW         = -90.0f;
            static constexpr float PITCH       =  0.0f;
            static constexpr float SPEED       =  2.5f;
            static constexpr float SENSITIVITY =  0.1f;
            static constexpr float ZOOM        =  45.0f;

            /* Calculates the front vector from the Camera's
             * (updated) Euler Angles.
             */
            void updateCameraVectors();
    };
}
