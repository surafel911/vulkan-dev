#pragma once

#include <memory>
#include <cstdlib>
#include <string_view>

namespace vulkanexamples::base {
    struct free_deleter {
        void operator () (void* ptr) const
        {
            std::free(ptr);
        }
    };

    class Image {
        private:
            Image() = delete;

        public:
            static int
            Load(std::string_view path, int& x, int& y, std::unique_ptr<std::byte[], free_deleter>& image);
    };
}