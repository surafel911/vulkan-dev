#pragma once

#include <array>

#include <glm/glm.hpp>

#include <vulkan/vulkan.h>

#include <base/Camera.hpp>
#include <base/VulkanExampleBase.hpp>

namespace vulkanexamples::examples {
    struct Vertex {
        glm::vec3 pos {};
        glm::vec3 normal {};

        Vertex() = default;
        constexpr Vertex(const glm::vec3& pos, const glm::vec3& normal)
            : pos{pos}, normal{normal}
        { }
    };

    struct SceneUBO {
        alignas(16) glm::mat4 view {};
        alignas(16) glm::mat4 proj {};

        SceneUBO() = default;
        SceneUBO(const glm::mat4& view, const glm::mat4& proj)
            : view(view), proj(proj)
        { }
    };

    struct CubeUBO {
        static constexpr std::array<Vertex, 36> vertices {{
            {{0.5f,  0.5f, -0.5f}, {0.0f,  0.0f, -1.0f}}, 
            {{0.5f, -0.5f, -0.5f}, {0.0f,  0.0f, -1.0f}}, 
            {{-0.5f, -0.5f, -0.5f}, {0.0f,  0.0f, -1.0f}},
            {{0.5f,  0.5f, -0.5f}, {0.0f,  0.0f, -1.0f}}, 
            {{-0.5f, -0.5f, -0.5f}, {0.0f,  0.0f, -1.0f}},
            {{-0.5f,  0.5f, -0.5f}, {0.0f,  0.0f, -1.0f}},  

            {{-0.5f, -0.5f,  0.5f}, {0.0f,  0.0f, 1.0f}},
            {{0.5f, -0.5f,  0.5f}, {0.0f,  0.0f, 1.0f}},
            {{0.5f,  0.5f,  0.5f}, {0.0f,  0.0f, 1.0f}},
            {{0.5f,  0.5f,  0.5f}, {0.0f,  0.0f, 1.0f}},
            {{-0.5f,  0.5f,  0.5f}, {0.0f,  0.0f, 1.0f}},
            {{-0.5f, -0.5f,  0.5f}, {0.0f,  0.0f, 1.0f}},

            {{-0.5f,  0.5f,  0.5f}, {-1.0f,  0.0f,  0.0f}},
            {{-0.5f,  0.5f, -0.5f}, {-1.0f,  0.0f,  0.0f}},
            {{-0.5f, -0.5f, -0.5f}, {-1.0f,  0.0f,  0.0f}},
            {{-0.5f, -0.5f, -0.5f}, {-1.0f,  0.0f,  0.0f}},
            {{-0.5f, -0.5f,  0.5f}, {-1.0f,  0.0f,  0.0f}},
            {{-0.5f,  0.5f,  0.5f}, {-1.0f,  0.0f,  0.0f}},

            {{0.5f,  0.5f,  0.5f},  {1.0f,  0.0f,  0.0f}},
            {{0.5f, -0.5f, -0.5f},  {1.0f,  0.0f,  0.0f}},
            {{0.5f,  0.5f, -0.5f},  {1.0f,  0.0f,  0.0f}},
            {{0.5f,  0.5f,  0.5f},  {1.0f,  0.0f,  0.0f}},
            {{0.5f, -0.5f,  0.5f},  {1.0f,  0.0f,  0.0f}},
            {{0.5f, -0.5f, -0.5f},  {1.0f,  0.0f,  0.0f}},

            {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f,  0.0f}},
            {{0.5f, -0.5f,  0.5f}, {0.0f, -1.0f,  0.0f}},
            {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f,  0.0f}},
            {{-0.5f, -0.5f,  0.5f}, {0.0f, -1.0f,  0.0f}},
            {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f,  0.0f}},
            {{0.5f, -0.5f,  0.5f}, {0.0f, -1.0f,  0.0f}},

            {{-0.5f,  0.5f, -0.5f}, {0.0f,  1.0f,  0.0f}},
            {{0.5f,  0.5f,  0.5f}, {0.0f,  1.0f,  0.0f}},
            {{0.5f,  0.5f, -0.5f}, {0.0f,  1.0f,  0.0f}},
            {{-0.5f,  0.5f, -0.5f}, {0.0f,  1.0f,  0.0}},
            {{-0.5f,  0.5f,  0.5f}, {0.0f,  1.0f,  0.0f}},
            {{0.5f,  0.5f,  0.5f}, {0.0f,  1.0f,  0.0f}},
        }};

        static constexpr std::array<std::uint16_t, 36> indices {
            // TOP
            0,1,2,
            0,2,3,
        
            // BOTTOM
            4,5,6,
            4,6,7,
        
            // LEFT
            8,9,10,
            8,10,11,
        
            // RIGHT
            12,13,14,
            12,14,15,
        
            // FRONT
            16,17,18,
            16,18,19,
        
            // BACK
            20,21,22,
            20,22,23
        };

        alignas(16) glm::mat4 model {};
        alignas(16) glm::vec4 color {};

        CubeUBO() = default;
        constexpr CubeUBO(const glm::mat4& model, const glm::vec4& color)
            : model{model}, color{color}
        { }
    };

    struct LightUBO {
        alignas(16) glm::vec4 pos {};
        alignas(16) glm::vec4 color {};

        LightUBO() = default;
        constexpr LightUBO(const glm::vec4& pos, const glm::vec4& color)
            : pos{pos}, color{color}
        { }
    };

    struct CameraUBO {
        alignas(16) glm::vec4 pos {};

        CameraUBO() = default;
        constexpr CameraUBO(const glm::vec4& pos)
            : pos{pos}
        { }
    };

    class VulkanBlinnPhongShading final : public base::VulkanExampleBase {
        public:
            VulkanBlinnPhongShading() : base::VulkanExampleBase() { 
                this->_settings.title = "Vulkan Blinn Phong Shading";
            }

            virtual ~VulkanBlinnPhongShading() { }

        private:
            static constexpr int NUM_OF_CUBES {2};

            VkDescriptorSetLayout _descriptor_set_layout {};

            VkPipelineLayout _pipeline_layout {};
            VkRenderPass _render_pass {};

            VkPipeline _solid_shading_graphics_pipeline {};
            VkPipeline _phong_shading_graphics_pipeline {};

            VkCommandPool _command_pool {};
            
            VkBuffer _cube_vertex_buffer {};
            VkDeviceMemory _cube_vertex_buffer_memory {};

            VkBuffer _cube_index_buffer {};
            VkDeviceMemory _cube_index_buffer_memory {};

            VkBuffer _staging_buffer {};
            VkDeviceMemory _staging_buffer_memory {};      

            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _scene_uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _scene_uniform_buffers_memory {};
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _scene_uniform_buffers_mapped_memory {};

            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _cube_uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _cube_uniform_buffers_memory {};
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _cube_uniform_buffers_mapped_memory {};

            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _light_uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _light_uniform_buffers_memory {};
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _light_uniform_buffers_mapped_memory {};


            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _camera_uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _camera_uniform_buffers_memory {};
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _camera_uniform_buffers_mapped_memory {};

            VkDescriptorPool _descriptor_pool {};
            std::vector<VkDescriptorSet> _descriptor_sets {};

            std::array<VkCommandBuffer, MAX_FRAMES_IN_FLIGHT> _primary_command_buffers {};
            std::vector<VkCommandBuffer> _secondary_command_buffers {};

            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _image_available_semaphores {};
            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _render_finished_semaphores {};
            std::array<VkFence, MAX_FRAMES_IN_FLIGHT> _in_flight_fences {};

            std::uint32_t _current_frame {};
            std::uint32_t _dynamic_alignment {};

            base::Camera _camera {};

            VkResult setup() override;
            void terminate() override;
            VkResult renderFrame() override;

            VkResult createDescriptorSetLayout();
            void destroyDescriptorSetLayout();

            VkResult createPipelineLayout();
            void destroyPipelineLayout();

            VkResult createRenderPass();
            void destroyRenderPass();

            VkResult createSwapChainFramebuffers();
            void destroySwapChainFramebuffers();

            VkResult recreateSwapChainResources();
   
            VkResult createGraphicsPipeline();
            void destroyGraphicsPipeline();

            VkResult createCommandPool();
            void destroyCommandPool();

            VkResult createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            void destroyBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            std::uint32_t findMemoryType(std::uint32_t type_filter, VkMemoryPropertyFlags properties);
            VkResult copyBuffers(VkBuffer src, VkBuffer dst, VkDeviceSize size);

            VkResult createVertexBuffer();
            void destroyVertexBuffer();

            VkResult createIndexBuffer();
            void destroyIndexBuffer();

            VkResult createUniformBuffers();
            void destroyUniformBuffers();
            void updateUniformBuffers();

            VkResult createDescriptorPool();
            void destroyDescriptorPool();

            VkResult createDescriptorSets();

            VkResult createPrimaryCommandBuffers();
            VkResult populatePrimaryCommandBuffers(uint32_t buffer_index, uint32_t image_index);

            VkResult createSecondaryCommandBuffers();
            VkResult populateSecondaryCommandBuffers();
            VkResult recreateSecondaryCommandBuffers();

            VkResult createSyncObjects();
            void destroySyncObjects();
    };
}