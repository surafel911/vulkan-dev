#pragma once

#include <array>

#include <vulkan/vulkan.h>

#include <base/VulkanExampleBase.hpp>

namespace vulkanexamples::examples {
    class VulkanTriangle final : public base::VulkanExampleBase {
        public:
            VulkanTriangle() : base::VulkanExampleBase()
            { 
                this->_settings.title = "Vulkan Triangle";
            }

            virtual ~VulkanTriangle() { }

        private:
            VkPipelineLayout _pipeline_layout {};
            VkRenderPass _render_pass {};
            VkPipeline _graphics_pipeline {};

            VkCommandPool _command_pool {};
            std::array<VkCommandBuffer, MAX_FRAMES_IN_FLIGHT> _command_buffers {};

            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _image_available_semaphores {};
            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _render_finished_semaphores {};
            std::array<VkFence, MAX_FRAMES_IN_FLIGHT> _in_flight_fences {};

            std::uint32_t _current_frame {};

            VkResult setup() override;
            void terminate() override;
            VkResult renderFrame() override;

            VkResult createPipelineLayout();
            void destroyPipelineLayout();

            VkResult createRenderPass();
            void destroyRenderPass();

            VkResult createSwapChainFramebuffers();
            void destroySwapChainFramebuffers();

            VkResult recreateSwapChainResources();

            VkResult createGraphicsPipeline();
            void destroyGraphicsPipeline();

            VkResult createCommandPool();
            void destroyCommandPool();

            VkResult createCommandBuffers();

            VkResult recordCommands(VkCommandBuffer command_buffer, uint32_t image_index);

            VkResult createSyncObjects();
            void destroySyncObjects();
    };
}