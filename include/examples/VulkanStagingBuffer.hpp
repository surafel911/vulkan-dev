#pragma once

#include <array>

#include <glm/glm.hpp>

#include <vulkan/vulkan.h>

#include <base/VulkanExampleBase.hpp>

namespace vulkanexamples::examples {
    struct Vertex {
        glm::vec2 pos {};
        glm::vec3 color {};

        Vertex() = default;
        Vertex(const glm::vec2& pos, const glm::vec3& color)
            : pos{pos}, color{color}
        { }
    };

    class VulkanStagingBuffer final : public base::VulkanExampleBase {
        public:
            VulkanStagingBuffer() : base::VulkanExampleBase()
            { 
                this->_settings.title = "Vulkan Staging Buffer";
            }

            virtual ~VulkanStagingBuffer() { }

        private:
            const std::vector<Vertex> vertices = {
                  // pos            // color
                {{0.0f, -0.5f}, {1.0f, 0.0f, 0.0f}},
                {{0.5f, 0.5f},  {0.0f, 1.0f, 0.0f}},
                {{-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}}
            };

            VkPipelineLayout _pipeline_layout {};
            VkRenderPass _render_pass {};
            VkPipeline _graphics_pipeline {};

            VkCommandPool _command_pool {};

            VkBuffer _vertex_buffer {};
            VkDeviceMemory _vertex_buffer_memory {};

            VkBuffer _staging_buffer {};
            VkDeviceMemory _staging_buffer_memory {};    

            std::array<VkCommandBuffer, MAX_FRAMES_IN_FLIGHT> _primary_command_buffers {};
            std::vector<VkCommandBuffer> _secondary_command_buffers {};

            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _image_available_semaphores {};
            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _render_finished_semaphores {};
            std::array<VkFence, MAX_FRAMES_IN_FLIGHT> _in_flight_fences {};
  
            std::uint32_t _current_frame {};

            VkResult setup() override;
            void terminate() override;

            VkResult createPipelineLayout();
            void destroyPipelineLayout();

            VkResult createRenderPass();
            void destroyRenderPass();

            VkResult createSwapChainFramebuffers();
            void destroySwapChainFramebuffers();

            VkResult recreateSwapChainResources();
   
            VkResult createGraphicsPipeline();
            void destroyGraphicsPipeline();

            VkResult createCommandPool();
            void destroyCommandPool();

            VkResult createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            void destroyBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            std::uint32_t findMemoryType(std::uint32_t type_filter, VkMemoryPropertyFlags properties);
            VkResult copyBuffers(VkBuffer src, VkBuffer dst, VkDeviceSize size);

            VkResult createVertexBuffer();
            void destroyVertexBuffer();

            VkResult createPrimaryCommandBuffers();
            VkResult populatePrimaryCommandBuffers(uint32_t buffer_index, uint32_t image_index);

            VkResult createSecondaryCommandBuffers();
            VkResult populateSecondaryCommandBuffers();
            VkResult recreateSecondaryCommandBuffers();

            VkResult createSyncObjects();
            void destroySyncObjects();
            
            VkResult renderFrame() override;
    };
}