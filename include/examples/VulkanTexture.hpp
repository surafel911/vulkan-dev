#pragma once

#include <array>

#include <glm/glm.hpp>

#include <vulkan/vulkan.h>

#include <base/VulkanExampleBase.hpp>

namespace vulkanexamples::examples {
    struct Vertex {
        glm::vec2 pos {};
        glm::vec4 color {};
        glm::vec2 texcoord {};

        Vertex() = default;
        Vertex(const glm::vec2& pos, const glm::vec4& color, const glm::vec2& texcoord)
            : pos{pos}, color{color}, texcoord{texcoord}
        { }
    };

    struct QuadUBO {
        glm::mat4 model {};
        glm::mat4 view {};
        glm::mat4 proj {};

        QuadUBO() = default;
        QuadUBO(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj)
            : model{model}, view{view}, proj{proj}
        { }
    };

    class VulkanTexture final : public base::VulkanExampleBase {
        public:
            VulkanTexture() : base::VulkanExampleBase()
            {
                this->_settings.title = "Vulkan Textures";
            }

            virtual ~VulkanTexture() { }

        private:
            const std::vector<Vertex> vertices = {
                  // pos             // color                   // texcoord
                {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
                {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},
                {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
                {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}
            };

            const std::vector<std::uint16_t> indices = {
                0, 1, 2, 2, 3, 0
            };

            VkDescriptorSetLayout _descriptor_set_layout {};

            VkPipelineLayout _pipeline_layout {};
            VkRenderPass _render_pass {};
            VkPipeline _graphics_pipeline {};

            VkCommandPool _command_pool {};

            VkBuffer _vertex_buffer {};
            VkDeviceMemory _vertex_buffer_memory {};

            VkBuffer _index_buffer {};
            VkDeviceMemory _index_buffer_memory {};

            VkBuffer _staging_buffer {};
            VkDeviceMemory _staging_buffer_memory {};

            VkImage _texture_image {};
            VkDeviceMemory _texture_image_memory {};

            VkImageView _texture_image_view {};

            VkSampler _texture_sampler {};

            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _uniform_buffers_memory {};

            // This technique is called "persistent mapping" and works on all Vulkan implementations. Not having to map the buffer every time we need to update it increases performances, as mapping is not free.
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _uniform_buffers_mapped_memory {};

            VkDescriptorPool _descriptor_pool {};
            std::vector<VkDescriptorSet> _descriptor_sets {};

            std::array<VkCommandBuffer, MAX_FRAMES_IN_FLIGHT> _primary_command_buffers {};
            std::vector<VkCommandBuffer> _secondary_command_buffers {};

            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _image_available_semaphores {};
            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _render_finished_semaphores {};
            std::array<VkFence, MAX_FRAMES_IN_FLIGHT> _in_flight_fences {};

            std::uint32_t _current_frame {};

            VkResult setup() override;
            void terminate() override;
            VkResult renderFrame() override;

            VkResult createDescriptorSetLayout();
            void destroyDescriptorSetLayout();

            VkResult createPipelineLayout();
            void destroyPipelineLayout();

            VkResult createRenderPass();
            void destroyRenderPass();

            VkResult createSwapChainFramebuffers();
            void destroySwapChainFramebuffers();

            VkResult recreateSwapChainResources();

            VkResult createGraphicsPipeline();
            void destroyGraphicsPipeline();

            VkResult createCommandPool();
            void destroyCommandPool();

            VkResult beginSingleUseCommandBuffer(VkCommandBuffer& command_buffer);
            VkResult endSingleUseCommandBuffer(VkCommandBuffer& command_buffer);

            VkResult createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            void destroyBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            std::uint32_t findMemoryType(std::uint32_t type_filter, VkMemoryPropertyFlags properties);
            VkResult copyHostToDevice(const void* src, VkDeviceMemory dst, std::size_t size);
            VkResult copyBuffers(VkBuffer src, VkBuffer dst, VkDeviceSize size);

            VkResult createVertexBuffer();
            void destroyVertexBuffer();

            VkResult createIndexBuffer();
            void destroyIndexBuffer();

            VkResult createUniformBuffers();
            void destroyUniformBuffers();
            void updateUniformBuffers();

            VkResult createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& image_memory);
            void destroyImage(VkImage& image, VkDeviceMemory& image_memory);
            VkResult transitionImageLayout(VkImage image, VkFormat format, VkImageLayout old_layout, VkImageLayout new_layout);
            VkResult copyBufferToImage(VkBuffer src, VkImage dst, uint32_t width, uint32_t height);

            VkResult createTextureImage();
            void destroyTextureImage();

            VkResult createImageView(VkImage image, VkFormat format, VkImageView& image_view);
            void destroyImageView(VkImageView& image_view);

            VkResult createTextureImageView();
            void destroyTextureImageView();

            VkResult createTextureSampler();
            void destroyTextureSampler();

            VkResult createDescriptorPool();
            void destroyDescriptorPool();

            VkResult createDescriptorSets();

            // The secondary command buffers must be populated *after* creating
            // all resources required for drawing. For simplicity, the creation
            // and population of primary and secondary command buffers are grouped
            // together.
            VkResult createPrimaryCommandBuffers();
            VkResult populatePrimaryCommandBuffers(uint32_t buffer_index, uint32_t image_index);

            VkResult createSecondaryCommandBuffers();
            VkResult populateSecondaryCommandBuffers();
            VkResult recreateSecondaryCommandBuffers();

            VkResult createSyncObjects();
            void destroySyncObjects();

    };
}
