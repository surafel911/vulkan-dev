#pragma once

#include <array>

#include <glm/glm.hpp>

#include <vulkan/vulkan.h>

#include <base/VulkanExampleBase.hpp>

namespace vulkanexamples::examples {
    struct Vertex {
        glm::vec2 pos {};
        glm::vec3 color {};

        Vertex() = default;
        Vertex(const glm::vec2& pos, const glm::vec3& color)
            : pos{pos}, color{color}
        { }
    };

    struct QuadUBO {
        glm::mat4 model {};
        glm::mat4 view {};
        glm::mat4 proj {};

        QuadUBO() = default;
        QuadUBO(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj)
            : model{model}, view{view}, proj{proj}
        { }
    };

    class VulkanUniformBuffer final : public base::VulkanExampleBase {
        public:
            VulkanUniformBuffer() : base::VulkanExampleBase()
            {
                this->_settings.title = "Vulkan Uniform Buffer";
            }

            virtual ~VulkanUniformBuffer() { }

        private:
            const std::vector<Vertex> vertices = {
                  // pos            // color
                {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
                {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
                {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
                {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
            };

            const std::vector<std::uint16_t> indices = {
                0, 1, 2, 2, 3, 0
            };

            VkDescriptorSetLayout _descriptor_set_layout {};

            VkPipelineLayout _pipeline_layout {};
            VkRenderPass _render_pass {};
            VkPipeline _graphics_pipeline {};

            VkCommandPool _command_pool {};

            VkBuffer _vertex_buffer {};
            VkDeviceMemory _vertex_buffer_memory {};

            VkBuffer _index_buffer {};
            VkDeviceMemory _index_buffer_memory {};

            VkBuffer _staging_buffer {};
            VkDeviceMemory _staging_buffer_memory {};

            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _uniform_buffers_memory {};

            // This technique is called "persistent mapping" and works on all Vulkan implementations. Not having to map the buffer every time we need to update it increases performances, as mapping is not free.
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _uniform_buffers_mapped_memory {};

            VkDescriptorPool _descriptor_pool {};
            std::vector<VkDescriptorSet> _descriptor_sets {};

            std::array<VkCommandBuffer, MAX_FRAMES_IN_FLIGHT> _primary_command_buffers {};
            std::vector<VkCommandBuffer> _secondary_command_buffers {};

            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _image_available_semaphores {};
            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _render_finished_semaphores {};
            std::array<VkFence, MAX_FRAMES_IN_FLIGHT> _in_flight_fences {};

            std::uint32_t _current_frame {};

            VkResult setup() override;
            void terminate() override;
            VkResult renderFrame() override;

            VkResult createDescriptorSetLayout();
            void destroyDescriptorSetLayout();

            VkResult createPipelineLayout();
            void destroyPipelineLayout();

            VkResult createRenderPass();
            void destroyRenderPass();

            VkResult createSwapChainFramebuffers();
            void destroySwapChainFramebuffers();

            VkResult recreateSwapChainResources();

            VkResult createGraphicsPipeline();
            void destroyGraphicsPipeline();

            VkResult createCommandPool();
            void destroyCommandPool();

            VkResult createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            void destroyBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            std::uint32_t findMemoryType(std::uint32_t type_filter, VkMemoryPropertyFlags properties);
            VkResult copyBuffers(VkBuffer src, VkBuffer dst, VkDeviceSize size);

            VkResult createVertexBuffer();
            void destroyVertexBuffer();

            VkResult createIndexBuffer();
            void destroyIndexBuffer();

            VkResult createUniformBuffers();
            void destroyUniformBuffers();
            void updateUniformBuffers();

            VkResult createDescriptorPool();
            void destroyDescriptorPool();

            VkResult createDescriptorSets();

            // The secondary command buffers must be populated *after* creating
            // all resources required for drawing. For simplicity, the creation
            // and population of primary and secondary command buffers are grouped
            // together.
            VkResult createPrimaryCommandBuffers();
            VkResult populatePrimaryCommandBuffers(uint32_t buffer_index, uint32_t image_index);

            VkResult createSecondaryCommandBuffers();
            VkResult populateSecondaryCommandBuffers();
            VkResult recreateSecondaryCommandBuffers();

            VkResult createSyncObjects();
            void destroySyncObjects();

    };
}
