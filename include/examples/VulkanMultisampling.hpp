#pragma once

#include <array>

#include <glm/glm.hpp>

#include <vulkan/vulkan.h>

#include <base/Mesh.hpp>
#include <base/Camera.hpp>
#include <base/VulkanExampleBase.hpp>

namespace vulkanexamples::examples {
    struct Vertex {
        glm::vec3 pos {};
        glm::vec4 color {};
        glm::vec2 texcoord {};

        Vertex() = default;
        Vertex(const glm::vec3& pos, const glm::vec4& color, const glm::vec2& texcoord)
            : pos{pos}, color{color}, texcoord{texcoord}
        { }
    };

    struct ModelUBO {
        glm::mat4 model {};
        glm::mat4 view {};
        glm::mat4 proj {};

        ModelUBO() = default;
        ModelUBO(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj)
            : model{model}, view{view}, proj{proj}
        { }
    };

    class VulkanMultisampling final : public base::VulkanExampleBase {
        public:
            VulkanMultisampling() : base::VulkanExampleBase() {
                this->_settings.title = "Vulkan MipMaps";
            }

            virtual ~VulkanMultisampling() { }

        private:
            base::Mesh _mesh {};

            std::vector<Vertex> _vertices {};
            std::vector<uint32_t> _indices {};

            base::Camera _camera {};

            VkSampleCountFlagBits _msaa_sample_count {};

            VkDescriptorSetLayout _descriptor_set_layout {};

            VkPipelineLayout _pipeline_layout {};
            VkRenderPass _render_pass {};
            VkPipeline _graphics_pipeline {};

            VkCommandPool _command_pool {};

            VkBuffer _vertex_buffer {};
            VkDeviceMemory _vertex_buffer_memory {};

            VkBuffer _index_buffer {};
            VkDeviceMemory _index_buffer_memory {};

            VkBuffer _staging_buffer {};
            VkDeviceMemory _staging_buffer_memory {};

            uint32_t _mip_levels {};
            VkImage _texture_image {};
            VkDeviceMemory _texture_image_memory {};
            VkImageView _texture_image_view {};

            VkImage _color_image {};
            VkDeviceMemory _color_image_memory {};
            VkImageView _color_image_view {};

            VkImage _depth_image {};
            VkDeviceMemory _depth_image_memory {};
            VkImageView _depth_image_view {};

            VkSampler _texture_sampler {};

            std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> _uniform_buffers {};
            std::array<VkDeviceMemory, MAX_FRAMES_IN_FLIGHT> _uniform_buffers_memory {};

            // This technique is called "persistent mapping" and works on all Vulkan implementations. Not having to map the buffer every time we need to update it increases performances, as mapping is not free.
            std::array<void*, MAX_FRAMES_IN_FLIGHT> _uniform_buffers_mapped_memory {};

            VkDescriptorPool _descriptor_pool {};
            std::vector<VkDescriptorSet> _descriptor_sets {};

            std::array<VkCommandBuffer, MAX_FRAMES_IN_FLIGHT> _primary_command_buffers {};
            std::vector<VkCommandBuffer> _secondary_command_buffers {};

            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _image_available_semaphores {};
            std::array<VkSemaphore, MAX_FRAMES_IN_FLIGHT> _render_finished_semaphores {};
            std::array<VkFence, MAX_FRAMES_IN_FLIGHT> _in_flight_fences {};

            std::uint32_t _current_frame {};

            VkResult setup() override;
            void terminate() override;
            VkResult renderFrame() override;
            virtual SDL_bool handleEvent(const SDL_Event& event) override;

            VkSampleCountFlagBits getMaxUsableSampleCount();

            VkResult createDescriptorSetLayout();
            void destroyDescriptorSetLayout();

            VkResult createPipelineLayout();
            void destroyPipelineLayout();

            VkResult createRenderPass();
            void destroyRenderPass();

            VkResult createGraphicsPipeline();
            void destroyGraphicsPipeline();

            VkResult createCommandPool();
            void destroyCommandPool();

            VkResult beginSingleUseCommandBuffer(VkCommandBuffer& command_buffer);
            VkResult endSingleUseCommandBuffer(VkCommandBuffer& command_buffer);

            VkResult createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            void destroyBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory);
            std::uint32_t findMemoryType(std::uint32_t type_filter, VkMemoryPropertyFlags properties);
            VkResult copyHostToDevice(const void* src, VkDeviceMemory dst, std::size_t size);
            VkResult copyBuffers(VkBuffer src, VkBuffer dst, VkDeviceSize size);

            int loadModel();

            VkResult createVertexBuffer();
            void destroyVertexBuffer();

            VkResult createIndexBuffer();
            void destroyIndexBuffer();

            VkResult createUniformBuffers();
            void destroyUniformBuffers();
            void updateUniformBuffers();

            VkResult createImage(std::uint32_t width, std::uint32_t height, std::uint32_t mip_levels, VkSampleCountFlagBits sample_count, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& image_memory);
            void destroyImage(VkImage& image, VkDeviceMemory& image_memory);
            VkResult transitionImageLayout(VkImage image, VkFormat format, std::uint32_t mip_levels, VkImageLayout old_layout, VkImageLayout new_layout);
            VkResult copyBufferToImage(VkBuffer src, VkImage dst, std::uint32_t width, std::uint32_t height);

            VkResult generateMipmaps(VkImage image,  VkFormat format, std::uint32_t width, std::uint32_t height, std::uint32_t mip_levels);

            VkResult createTextureImage();
            void destroyTextureImage();

            VkResult createImageView(VkImage image, VkFormat format, std::uint32_t mip_levels, VkImageAspectFlags aspect_flags, VkImageView& image_view);
            void destroyImageView(VkImageView& image_view);

            VkResult createTextureImageView();
            void destroyTextureImageView();

            VkResult createTextureSampler();
            void destroyTextureSampler();

            VkResult createColorResources();
            void destroyColorResources();

            VkFormat findSupportedDepthFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
            VkFormat findDepthFormat();
            bool hasStencilComponent(VkFormat format);

            VkResult createDepthResources();
            void destroyDepthResources();

            VkResult createSwapChainFramebuffers();
            void destroySwapChainFramebuffers();

            VkResult recreateSwapChainResources();

            VkResult createDescriptorPool();
            void destroyDescriptorPool();

            VkResult createDescriptorSets();

            // The secondary command buffers must be populated *after* creating
            // all resources required for drawing. For simplicity, the creation
            // and population of primary and secondary command buffers are grouped
            // together.
            VkResult createPrimaryCommandBuffers();
            VkResult populatePrimaryCommandBuffers(std::uint32_t buffer_index, std::uint32_t image_index);

            VkResult createSecondaryCommandBuffers();
            VkResult populateSecondaryCommandBuffers();
            VkResult recreateSecondaryCommandBuffers();

            VkResult createSyncObjects();
            void destroySyncObjects();
    };
}
