#version 450

layout(location = 0) in vec4 vertColor;
layout(location = 1) in vec2 vertTexCoord;

layout(binding = 1) uniform sampler2D texSampler;


layout(location = 0) out vec4 fragColor;

void main() {
    fragColor = texture(texSampler, vertTexCoord);
}