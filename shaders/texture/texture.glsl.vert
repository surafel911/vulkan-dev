#version 450

layout(set = 0, binding = 0) uniform QuadUBO {
    mat4 model;
    mat4 view;
    mat4 proj;
} quad_ubo;

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 fragTexCoord;

void main() {
    gl_Position = quad_ubo.proj * quad_ubo.view * quad_ubo.model * vec4(inPosition, 0.0, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
}
