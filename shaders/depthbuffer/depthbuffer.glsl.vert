#version 450

layout(set = 0, binding = 0) uniform ObjectUBO {
    mat4 model;
    mat4 view;
    mat4 proj;
} object_ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 fragTexCoord;

void main() {
    gl_Position = object_ubo.proj * object_ubo.view * object_ubo.model * vec4(inPosition, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
}
