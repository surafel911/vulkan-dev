#version 450

layout(set = 0, binding = 0) uniform SceneUBO {
    mat4 view;
    mat4 proj;
} scene_ubo;

layout(set = 0, binding = 1) uniform CubeUBO {
    mat4 view;
    vec4 color;
} cube_ubo;

layout(set = 0, binding = 2) uniform LightUBO {
    vec4 pos;
    vec4 color;
} light_ubo;

layout(location = 0) in vec3 in_frag_pos;
layout(location = 1) in vec3 in_normal;

layout(location = 0) out vec4 out_frag_color;


void main() {
    out_frag_color = light_ubo.color;
}