#version 450

layout(set = 0, binding = 0) uniform SceneUBO {
    mat4 view;
    mat4 proj;
} scene_ubo;

layout(set = 0, binding = 1) uniform CubeUBO {
    mat4 model;
    vec4 color;
} cube_ubo;

layout(set = 0, binding = 2) uniform LightUBO {
    vec4 pos;
    vec4 color;
} light_ubo;

layout(set = 0, binding = 3) uniform CameraUBO {
    vec4 pos;
} camera_ubo;

layout(location = 0) in vec3 in_frag_pos;
layout(location = 1) in vec3 in_normal;

layout(location = 0) out vec4 out_frag_color;

void main() {
    float ambient_strength = 0.05;
    vec3 ambient = ambient_strength * vec3(light_ubo.color);

    vec3 normal = normalize(in_normal);
    vec3 light_dir = normalize(light_ubo.pos.xyz - in_frag_pos);
    float lambert = max(0.0, dot(normal, light_dir));
    vec3 lambertian = lambert * vec3(light_ubo.color);

    float specular_strength = 10.0;
    vec3 view_dir = normalize(vec3(camera_ubo.pos) - in_frag_pos);
    vec3 reflect_dir = reflect(-light_dir, normal);

    float shininess = 256;
    float spec = pow(max(dot(reflect_dir, view_dir), 0.0), shininess);
    vec3 specular = specular_strength * spec * vec3(light_ubo.color);

    vec3 result = (ambient + lambertian + specular) * vec3(cube_ubo.color);
    out_frag_color = vec4(result, 1.0);
}