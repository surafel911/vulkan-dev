#version 450

layout(set = 0, binding = 0) uniform SceneUBO {
    mat4 view;
    mat4 proj;
} scene_ubo;

layout(set = 0, binding = 1) uniform CubeUBO {
    mat4 model;
    vec4 color;
} cube_ubo;

layout(set = 0, binding = 2) uniform LightUBO {
    vec4 pos;
    vec4 color;
} light_ubo;


layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;

layout(location = 0) out vec3 out_frag_pos;
layout(location = 1) out vec3 out_normal;

void main() {
    out_frag_pos = vec3(cube_ubo.model * vec4(in_pos, 1.0));
    out_normal = vec3(mat3(cube_ubo.model) * in_normal);

    gl_Position = scene_ubo.proj * scene_ubo.view * vec4(out_frag_pos, 1.0);
}