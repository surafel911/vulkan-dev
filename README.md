# Vulkan-Practice

This is a simple project learn using Vulkan. I am not attempting production-quality code here.

## Resources

### Vulkan
- https://vulkan-tutorial.com/
- https://vkguide.dev/docs/new_vkguide/
- https://github.com/KhronosGroup/Vulkan-Docs/wiki/Synchronization-Examples#swapchain-image-acquire-and-present
 - https://www.youtube.com/watch?v=e14z9oOsPu0
 - https://youtube.com/playlist?list=PLmIqTlJ6KsE1Jx5HV4sd2jOe3V1KMHHgn&si=w34_4KEYjX-WxqrK 
 - https://themaister.net/blog/2017/08/15/render-graphs-and-vulkan-a-deep-dive/
 - https://www.gdcvault.com/play/1024612/FrameGraph-Extensible-Rendering-Architecture-in
 - https://zeux.io/2020/02/27/writing-an-efficient-vulkan-renderer/
 - https://github.com/KhronosGroup/Vulkan-Samples/tree/main/samples
 - https://github.com/charles-lunarg/vk-bootstrap/blob/main/docs/getting_started.md
 - https://github.com/KhronosGroup/Vulkan-Hpp/tree/main/RAII_Samples
 - https://github.com/KhronosGroup/Vulkan-Hpp/blob/main/vk_raii_ProgrammingGuide.md

### SDL
- https://lazyfoo.net/tutorials/SDL/

### Rust
- https://hoj-senna.github.io/ashen-aetna/
- https://docs.rs/ash/latest/ash/index.html
