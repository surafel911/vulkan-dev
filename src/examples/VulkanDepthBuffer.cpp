#include <examples/VulkanDepthBuffer.hpp>

#include <array>
#include <chrono>
#include <limits>
#include <cstdint>
#include <cstdlib>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vulkan/vulkan.h>

#include <base/Image.hpp>
#include <base/VulkanExampleBase.hpp>

VkResult
vulkanexamples::examples::VulkanDepthBuffer::setup() {
    this->_current_frame = 0;

    VK_CHECK(this->createDescriptorSetLayout());
    VK_CHECK(this->createPipelineLayout());
    VK_CHECK(this->createRenderPass());
    VK_CHECK(this->createGraphicsPipeline());
    VK_CHECK(this->createCommandPool());
    VK_CHECK(this->createVertexBuffer());
    VK_CHECK(this->createIndexBuffer());
    VK_CHECK(this->createUniformBuffers());
    VK_CHECK(this->createTextureImage());
    VK_CHECK(this->createTextureImageView());
    VK_CHECK(this->createTextureSampler());

    VK_CHECK(this->createDepthResources());
    VK_CHECK(this->createSwapChainFramebuffers());

    VK_CHECK(this->createDescriptorPool());
    VK_CHECK(this->createDescriptorSets());
    VK_CHECK(this->createPrimaryCommandBuffers());
    VK_CHECK(this->createSecondaryCommandBuffers());
    VK_CHECK(this->populateSecondaryCommandBuffers());
    VK_CHECK(this->createSyncObjects());

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::terminate() {
    this->destroySyncObjects();
    this->destroyDescriptorPool();

    this->destroySwapChainFramebuffers();
    this->destroyDepthResources();

    this->destroyTextureSampler();
    this->destroyTextureImageView();
    this->destroyTextureImage();
    this->destroyUniformBuffers();
    this->destroyIndexBuffer();
    this->destroyVertexBuffer();
    this->destroyCommandPool();
    this->destroyGraphicsPipeline();
    this->destroyRenderPass();
    this->destroyPipelineLayout();
    this->destroyDescriptorSetLayout();
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createDescriptorSetLayout() {
    VkDescriptorSetLayoutBinding ubo_descriptor_set_layout_binding {};
    ubo_descriptor_set_layout_binding.binding = 0;
    ubo_descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    ubo_descriptor_set_layout_binding.descriptorCount = 1;
    ubo_descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    ubo_descriptor_set_layout_binding.pImmutableSamplers = nullptr; // Optional

    VkDescriptorSetLayoutBinding sampler_descriptor_set_layout_binding {};
    sampler_descriptor_set_layout_binding.binding = 1;
    sampler_descriptor_set_layout_binding.descriptorCount = 1;
    sampler_descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    sampler_descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    sampler_descriptor_set_layout_binding.pImmutableSamplers = nullptr;

    std::array<VkDescriptorSetLayoutBinding, 2> descriptor_set_layout_bindings {
        ubo_descriptor_set_layout_binding,
        sampler_descriptor_set_layout_binding
        };

    VkDescriptorSetLayoutCreateInfo descriptor_set_layout_create_info {};
    descriptor_set_layout_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptor_set_layout_create_info.bindingCount = descriptor_set_layout_bindings.size();
    descriptor_set_layout_create_info.pBindings = descriptor_set_layout_bindings.data();

    VK_CHECK(vkCreateDescriptorSetLayout(this->_device, &descriptor_set_layout_create_info, nullptr, &this->_descriptor_set_layout));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyDescriptorSetLayout() {
    vkDestroyDescriptorSetLayout(this->_device, this->_descriptor_set_layout, nullptr);
    this->_descriptor_set_layout = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createPipelineLayout() {
    // The pipeline layout specifies any uniform or push constant
    // values in the shaders. Since we are not using either, zero
    // out the structure and create the pipeine layout.

    VkPipelineLayoutCreateInfo pipeline_layout_create_info {};
    pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_create_info.setLayoutCount = 1;
    pipeline_layout_create_info.pSetLayouts = &this->_descriptor_set_layout;
    pipeline_layout_create_info.pushConstantRangeCount = 0; // Optional
    pipeline_layout_create_info.pPushConstantRanges = nullptr; // Optional

    VK_CHECK(vkCreatePipelineLayout(this->_device, &pipeline_layout_create_info, nullptr, &this->_pipeline_layout));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyPipelineLayout() {
    vkDestroyPipelineLayout(this->_device, this->_pipeline_layout, nullptr);
    this->_pipeline_layout = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createRenderPass() {
    // We need to tell Vulkan about the framebuffer attachments
    // that will be used while rendering. We need to specify how
    // many color and depth buffers there will be, how many samples
    // to use for each of them and how their contents should be
    // handled throughout the rendering operations. All of this
    // information is wrapped in a render pass object,
    //
    // In this example, the color buffer is the only attachment
    // represented by one of the images from the swap chain.
    VkAttachmentDescription color_attachment_description {};
    color_attachment_description.format = this->_swap_chain_details.format.format;

    // Not using multisampling, so use only one sample.
    color_attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;

    // The loadOp and storeOp determine what to do with the data
    // in the attachment before rendering and after rendering.
    color_attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    color_attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

    // Not using any stencil data, ignore
    color_attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    color_attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    // Do not care about the layout (i.e. pixel format) of a VkImage
    // before rendering, but this does not matter since it is being
    // written over anyways. The image will be used for rendering,
    // so use the present layout.
    color_attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    color_attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentDescription depth_attachment_description {};
    depth_attachment_description.format = this->findDepthFormat();
    depth_attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;
    depth_attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depth_attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth_attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depth_attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth_attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depth_attachment_description.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    std::array<VkAttachmentDescription, 2> attachment_descriptions {
        color_attachment_description,
        depth_attachment_description
        };

    // Subpasses are subsequent rendering operations that depend on the
    // contents of framebuffers in previous passes, for example a sequence
    // of post-processing effects that are applied one after another.
    //
    // In other words, subpasses in a render pass automatically take care
    // of image layout transitions for before, during, and after rendering.
    //
    // In this example, we will only use one subpass for the main render pass.
    //
    // Each subpass references one or more attachments, which is defined below.
    VkAttachmentReference color_attachment_reference {};

    // This is the index into the attachment description array
    // this reference applies to.
    color_attachment_reference.attachment = 0;
    color_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depth_attachment_reference {};
    depth_attachment_reference.attachment = 1;
    depth_attachment_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass_description {};
    subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

    // The index of the attachment in this array is directly referenced from
    // the fragment shader with the layout(location = 0) out vec4 outColor
    // directive.
    subpass_description.colorAttachmentCount = 1;
    subpass_description.pColorAttachments = &color_attachment_reference;

    subpass_description.pDepthStencilAttachment = &depth_attachment_reference;

    // The image layout transisions are c ontrolled by the subpass
    // dependency. They tell the render pass how to handle an image
    // transition. The render pass is the only subpass we define, but the
    // transition before and after the render pass count as "implicit"
    // subpasses.
    //
    // The problem is that the starting subpass will transition the images
    // before we have aquired a swap chain image, therefore we will explicitly
    // create a subpass dependency to block the render pass until the image is
    // available
    VkSubpassDependency subpass_dependency {};

    // The special value VK_SUBPASS_EXTERNAL refers to the implicit subpass
    // before or after the render pass depending on whether it is specified
    // in srcSubpass or dstSubpass.
    subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;

    // The index 0 refers to our subpass, which is the first and only one.
    subpass_dependency.dstSubpass = 0;

    // specify the operations to wait on and the stages in which these
    // operations occur. We need to wait for the swap chain to finish reading
    // from the image before we can access it. This can be accomplished by waiting on the color attachment output stage
    subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    subpass_dependency.srcAccessMask = 0;

    // The operations that should wait on this are in the color attachment
    // stage and involve the writing of the color attachment. These settings
    // will prevent the transition from happening until it's actually necessary
    subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    VkRenderPassCreateInfo renderPassInfo {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = attachment_descriptions.size();
    renderPassInfo.pAttachments = attachment_descriptions.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass_description;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &subpass_dependency;

    VK_CHECK(vkCreateRenderPass(this->_device, &renderPassInfo, nullptr, &this->_render_pass));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyRenderPass() {
    vkDestroyRenderPass(this->_device, this->_render_pass, nullptr);
    this->_render_pass = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createGraphicsPipeline() {
    std::array dynamic_states {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
        };

    VkPipelineDynamicStateCreateInfo dynamic_state_create_info {};
    dynamic_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state_create_info.dynamicStateCount = static_cast<uint32_t>(dynamic_states.size());
    dynamic_state_create_info.pDynamicStates = dynamic_states.data();

    std::vector<std::byte> vertex_buffer {};
    VK_CHECK_R(readFile("shaders/depthbuffer/depthbuffer.spirv.vert", vertex_buffer), VK_ERROR_INVALID_SHADER_NV);

    std::vector<std::byte> fragment_buffer {};
    VK_CHECK_R(readFile("shaders/depthbuffer/depthbuffer.spirv.frag", fragment_buffer), VK_ERROR_INVALID_SHADER_NV);

    VkShaderModule vertex_shader_module {};
    VK_CHECK(this->createShaderModule(vertex_buffer, vertex_shader_module));

    VkShaderModule fragment_shader_module {};
    VK_CHECK(this->createShaderModule(fragment_buffer, fragment_shader_module));

    // A vertex binding describes at which rate to load data from memory
    // throughout the vertices. It specifies the number of bytes between
    // data entries and whether to move to the next data entry after each
    // vertex or after each instance
    VkVertexInputBindingDescription vertex_input_binding_description {};
    vertex_input_binding_description.binding = 0;
    vertex_input_binding_description.stride = sizeof(Vertex);
    vertex_input_binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    std::array<VkVertexInputAttributeDescription, 3> vertex_input_attribute_description {};

    vertex_input_attribute_description[0].binding = 0;
    vertex_input_attribute_description[0].location = 0;
    vertex_input_attribute_description[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertex_input_attribute_description[0].offset = offsetof(Vertex, pos);

    vertex_input_attribute_description[1].binding = 0;
    vertex_input_attribute_description[1].location = 1;
    vertex_input_attribute_description[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vertex_input_attribute_description[1].offset = offsetof(Vertex, color);

    vertex_input_attribute_description[2].binding = 0;
    vertex_input_attribute_description[2].location = 2;
    vertex_input_attribute_description[2].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertex_input_attribute_description[2].offset = offsetof(Vertex, texcoord);

    VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info {};
    vertex_input_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertex_input_state_create_info.vertexBindingDescriptionCount = 1;
    vertex_input_state_create_info.pVertexBindingDescriptions = &vertex_input_binding_description;
    vertex_input_state_create_info.vertexAttributeDescriptionCount = vertex_input_attribute_description.size();
    vertex_input_state_create_info.pVertexAttributeDescriptions = vertex_input_attribute_description.data();

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info {};
    input_assembly_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    input_assembly_state_create_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    input_assembly_state_create_info.primitiveRestartEnable = VK_FALSE;

    std::array<VkPipelineShaderStageCreateInfo, 2> shader_stages_create_info {};

    shader_stages_create_info[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shader_stages_create_info[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    shader_stages_create_info[0].module = vertex_shader_module;
    shader_stages_create_info[0].pName = "main";

    shader_stages_create_info[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shader_stages_create_info[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    shader_stages_create_info[1].module = fragment_shader_module;
    shader_stages_create_info[1].pName = "main";

    VkPipelineViewportStateCreateInfo viewport_state_create_info {};
    viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state_create_info.viewportCount = 1;
    viewport_state_create_info.scissorCount = 1;

    VkPipelineRasterizationStateCreateInfo rasterizer_state_create_info {};
    rasterizer_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer_state_create_info.depthClampEnable = VK_FALSE;
    rasterizer_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    rasterizer_state_create_info.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer_state_create_info.lineWidth = 1.0f;

    // because of the Y-flip we did in the projection matrix, the vertices are now being drawn in counter-clockwise order instead of clockwise order. This causes backface culling to kick in and prevents any geometry from being drawn.
    rasterizer_state_create_info.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer_state_create_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

    rasterizer_state_create_info.depthBiasEnable = VK_FALSE;
    rasterizer_state_create_info.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer_state_create_info.depthBiasClamp = 0.0f; // Optional
    rasterizer_state_create_info.depthBiasSlopeFactor = 0.0f; // Optional

    VkPipelineMultisampleStateCreateInfo multisampling_state_create_info{};
    multisampling_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling_state_create_info.sampleShadingEnable = VK_FALSE;
    multisampling_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling_state_create_info.minSampleShading = 1.0f; // Optional
    multisampling_state_create_info.pSampleMask = nullptr; // Optional
    multisampling_state_create_info.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling_state_create_info.alphaToOneEnable = VK_FALSE; // Optional

    /* Depth testing still needs to be enabled in the graphics
     * pipeline. It is configured through the
     * VkPipelineDepthStencilStateCreateInfo struct:
     */
    VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info {};
    depth_stencil_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depth_stencil_state_create_info.depthTestEnable = VK_TRUE;
    depth_stencil_state_create_info.depthWriteEnable = VK_TRUE;

    /* The depthCompareOp field specifies the comparison that
     * is performed to keep or discard fragments. We're
     * sticking to the convention of lower depth = closer,
     * so the depth of new fragments should be less.
     */
    depth_stencil_state_create_info.depthCompareOp = VK_COMPARE_OP_LESS;


    /* The depthBoundsTestEnable, minDepthBounds and maxDepthBounds
     * fields are used for the optional depth bound test.
     * Basically, this allows you to only keep fragments that fall
     * within the specified depth range.
     */
    depth_stencil_state_create_info.depthBoundsTestEnable = VK_FALSE;
    depth_stencil_state_create_info.minDepthBounds = 0.0f; // Optional
    depth_stencil_state_create_info.maxDepthBounds = 1.0f; // Optional

    /* The last three fields configure stencil buffer operations,
     * which we also won't be using in this tutorial. If you
     * want to use these operations, then you will have to make
     * sure that the format of the depth/stencil image
     * contains a stencil component
     */
    depth_stencil_state_create_info.stencilTestEnable = VK_FALSE;
    depth_stencil_state_create_info.front = {}; // Optional
    depth_stencil_state_create_info.back = {}; // Optional

    /* Configuration per-attached framebuffer. There is only one framebuffer
     * so we only have one attachment state struct.
     *
     * This configuration will discard the color in the framebuffer with the
     * color produced from the fragment shader.
     */
    VkPipelineColorBlendAttachmentState color_blend_attachment_state {};
    color_blend_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    color_blend_attachment_state.blendEnable = VK_FALSE;
    color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    /* Contains the global color blending settings.
     *
     * References the array of blend attachment states for all of the framebuffers
     * and sets blend constants that for use as blend factors in the
     * blending calculations.
     */
    VkPipelineColorBlendStateCreateInfo color_blending_state_create_info {};
    color_blending_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blending_state_create_info.logicOpEnable = VK_FALSE;
    color_blending_state_create_info.logicOp = VK_LOGIC_OP_COPY; // Optional
    color_blending_state_create_info.attachmentCount = 1;
    color_blending_state_create_info.pAttachments = &color_blend_attachment_state;
    color_blending_state_create_info.blendConstants[0] = 0.0f; // Optional
    color_blending_state_create_info.blendConstants[1] = 0.0f; // Optional
    color_blending_state_create_info.blendConstants[2] = 0.0f; // Optional
    color_blending_state_create_info.blendConstants[3] = 0.0f; // Optional

    VkGraphicsPipelineCreateInfo graphics_pipeline_create_info {};
    graphics_pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphics_pipeline_create_info.stageCount = shader_stages_create_info.size();
    graphics_pipeline_create_info.pStages = shader_stages_create_info.data();

    graphics_pipeline_create_info.pVertexInputState = &vertex_input_state_create_info;
    graphics_pipeline_create_info.pInputAssemblyState = &input_assembly_state_create_info;
    graphics_pipeline_create_info.pViewportState = &viewport_state_create_info;
    graphics_pipeline_create_info.pRasterizationState = &rasterizer_state_create_info;
    graphics_pipeline_create_info.pMultisampleState = &multisampling_state_create_info;
    graphics_pipeline_create_info.pDepthStencilState = &depth_stencil_state_create_info;
    graphics_pipeline_create_info.pColorBlendState = &color_blending_state_create_info;
    graphics_pipeline_create_info.pDynamicState = &dynamic_state_create_info;

    graphics_pipeline_create_info.layout = this->_pipeline_layout;

    graphics_pipeline_create_info.renderPass = this->_render_pass;
    graphics_pipeline_create_info.subpass = 0;

    graphics_pipeline_create_info.basePipelineHandle = VK_NULL_HANDLE; // Optional
    graphics_pipeline_create_info.basePipelineIndex = -1; // Optional

    VK_CHECK(vkCreateGraphicsPipelines(this->_device, VK_NULL_HANDLE, 1, &graphics_pipeline_create_info, nullptr, &this->_graphics_pipeline));

    vkDestroyShaderModule(this->_device, fragment_shader_module, nullptr);

    vkDestroyShaderModule(this->_device, vertex_shader_module, nullptr);


    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyGraphicsPipeline() {
    vkDestroyPipeline(this->_device, this->_graphics_pipeline, nullptr);
    this->_graphics_pipeline = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createCommandPool() {
    VkCommandPoolCreateInfo command_pool_create_info {};
    command_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_pool_create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    command_pool_create_info.queueFamilyIndex = this->_queue_family_indices.graphics_queue_family.value();

    VK_CHECK(vkCreateCommandPool(this->_device, &command_pool_create_info, nullptr, &this->_command_pool));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyCommandPool() {
    vkDestroyCommandPool(this->_device, this->_command_pool, nullptr);
    this->_command_pool = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::beginSingleUseCommandBuffer(VkCommandBuffer& command_buffer) {
    VkCommandBufferAllocateInfo command_buffer_allocate_info {};
    command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    command_buffer_allocate_info.commandPool = this->_command_pool;
    command_buffer_allocate_info.commandBufferCount = 1;

    VK_CHECK(vkAllocateCommandBuffers(this->_device, &command_buffer_allocate_info, &command_buffer));

    VkCommandBufferBeginInfo command_buffer_begin_info {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VK_CHECK(vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::endSingleUseCommandBuffer(VkCommandBuffer& command_buffer) {
    VK_CHECK(vkEndCommandBuffer(command_buffer));

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &command_buffer;

    (void)vkQueueSubmit(this->_graphics_queue, 1, &submitInfo, VK_NULL_HANDLE);

    (void)vkQueueWaitIdle(this->_graphics_queue);

    // Does this command buffer not auto clean up like those for draw commands?
    vkFreeCommandBuffers(this->_device, this->_command_pool, 1, &command_buffer);

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& buffer_memory) {
    VkBufferCreateInfo buffer_create_info {};
    buffer_create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_create_info.size = size;
    buffer_create_info.usage = usage;
    buffer_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VK_CHECK(vkCreateBuffer(this->_device, &buffer_create_info, nullptr, &buffer));

    VkMemoryRequirements memory_requirements {};
    vkGetBufferMemoryRequirements(this->_device, buffer, &memory_requirements);

    VkMemoryAllocateInfo memory_allocate_info {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.allocationSize = memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = this->findMemoryType(memory_requirements.memoryTypeBits, properties);

    VK_CHECK(vkAllocateMemory(this->_device, &memory_allocate_info, nullptr, &buffer_memory))

    VK_CHECK(vkBindBufferMemory(this->_device, buffer, buffer_memory, 0));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory) {
    vkFreeMemory(this->_device, buffer_memory, nullptr);
    buffer_memory = VK_NULL_HANDLE;

    vkDestroyBuffer(this->_device, buffer, nullptr);
    buffer = VK_NULL_HANDLE;
}

std::uint32_t
vulkanexamples::examples::VulkanDepthBuffer::findMemoryType(std::uint32_t type_filter, VkMemoryPropertyFlags memory_properties) {
    VkPhysicalDeviceMemoryProperties physical_device_memory_properties {};
    vkGetPhysicalDeviceMemoryProperties(this->_physical_device, &physical_device_memory_properties);

    for (std::uint32_t i = 0; i < physical_device_memory_properties.memoryTypeCount; i++) {
        if ((type_filter & (1 << i))
            && (physical_device_memory_properties.memoryTypes[i].propertyFlags & memory_properties) == memory_properties) {
            return i;
        }
    }

    return std::numeric_limits<std::uint32_t>::max();
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::copyHostToDevice(const void* src, VkDeviceMemory dst, std::size_t size) {
    void* data {};

    VK_CHECK(vkMapMemory(this->_device, dst, 0, size, 0, &data));
    (void)memcpy(data, src, size);
    vkUnmapMemory(this->_device, dst);

    return VK_SUCCESS;
}


VkResult
vulkanexamples::examples::VulkanDepthBuffer::copyBuffers(VkBuffer src, VkBuffer dst, VkDeviceSize size) {
    VkCommandBuffer command_buffer {};
    VK_CHECK(this->beginSingleUseCommandBuffer(command_buffer));

    VkBufferCopy buffer_copy_region {};
    buffer_copy_region.srcOffset = 0; // Optional
    buffer_copy_region.dstOffset = 0; // Optional
    buffer_copy_region.size = size;

    vkCmdCopyBuffer(command_buffer, src, dst, 1, &buffer_copy_region);

    VK_CHECK(this->endSingleUseCommandBuffer(command_buffer));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createVertexBuffer() {
    VkDeviceSize size {sizeof(VulkanDepthBuffer::vertices[0]) * VulkanDepthBuffer::vertices.size()};

    VK_CHECK(this->createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, this->_staging_buffer, this->_staging_buffer_memory));

    VK_CHECK(this->copyHostToDevice(VulkanDepthBuffer::vertices.data(), this->_staging_buffer_memory, size));

    VK_CHECK(this->createBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->_vertex_buffer, this->_vertex_buffer_memory));

    VK_CHECK(this->copyBuffers(this->_staging_buffer, this->_vertex_buffer, size));

    this->destroyBuffer(this->_staging_buffer, this->_staging_buffer_memory);

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyVertexBuffer() {
    this->destroyBuffer(this->_vertex_buffer, this->_vertex_buffer_memory);
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createIndexBuffer() {
    VkDeviceSize size {sizeof(VulkanDepthBuffer::indices[0]) * VulkanDepthBuffer::indices.size()};

    VK_CHECK(this->createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, this->_staging_buffer, this->_staging_buffer_memory));

    VK_CHECK(this->copyHostToDevice(VulkanDepthBuffer::indices.data(), this->_staging_buffer_memory, size));

    VK_CHECK(this->createBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->_index_buffer, this->_index_buffer_memory));

    VK_CHECK(this->copyBuffers(this->_staging_buffer, this->_index_buffer, size));

    this->destroyBuffer(this->_staging_buffer, this->_staging_buffer_memory);

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyIndexBuffer() {
    this->destroyBuffer(this->_index_buffer, this->_index_buffer_memory);
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createUniformBuffers() {
    VkDeviceSize size {sizeof(QuadUBO)};
    for (int i = 0; i < VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT; ++i) {
        VK_CHECK(this->createBuffer(size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, this->_uniform_buffers[i], this->_uniform_buffers_memory[i]));

        VK_CHECK(vkMapMemory(this->_device, this->_uniform_buffers_memory[i], 0, size, 0, &this->_uniform_buffers_mapped_memory[i]));
    }

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyUniformBuffers() {
    for (int i = 0; i < VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT; ++i) {
        vkUnmapMemory(this->_device, this->_uniform_buffers_memory[i]);
        this->_uniform_buffers_mapped_memory[i] = nullptr;

        this->destroyBuffer(this->_uniform_buffers[i], this->_uniform_buffers_memory[i]);
    }
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& image_memory) {
    VkImageCreateInfo image_create_info {};
    image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create_info.imageType = VK_IMAGE_TYPE_2D;
    image_create_info.extent.width = static_cast<uint32_t>(width);
    image_create_info.extent.height = static_cast<uint32_t>(height);
    image_create_info.extent.depth = 1;
    image_create_info.mipLevels = 1;
    image_create_info.arrayLayers = 1;
    image_create_info.format = format;
    image_create_info.tiling = tiling;
    image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    image_create_info.usage = usage;
    image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VK_CHECK(vkCreateImage(this->_device, &image_create_info, nullptr, &image));

    VkMemoryRequirements memory_requirements {};
    vkGetImageMemoryRequirements(this->_device, image, &memory_requirements);

    VkMemoryAllocateInfo memory_allocate_info {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.allocationSize = memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = this->findMemoryType(memory_requirements.memoryTypeBits, properties);

    VK_CHECK(vkAllocateMemory(this->_device, &memory_allocate_info, nullptr, &image_memory));

    VK_CHECK(vkBindImageMemory(this->_device, image, image_memory, 0));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyImage(VkImage& image, VkDeviceMemory& image_memory) {
    vkDestroyImage(this->_device, image, nullptr);
    image = VK_NULL_HANDLE;

    vkFreeMemory(this->_device, image_memory, nullptr);
    image_memory = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::transitionImageLayout(VkImage image, VkFormat format, VkImageLayout old_layout, VkImageLayout new_layout) {
    // For use in future chapters
    (void)format;

    VkCommandBuffer command_buffer {};
    VK_CHECK(this->beginSingleUseCommandBuffer(command_buffer));

    VkImageMemoryBarrier image_memory_barrier {};
    image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;

    /* Alternatively can be VK_IMAGE_LAYOUT_UNDEFINED if you do
     * not care about the old layout state.
     */
    image_memory_barrier.oldLayout = old_layout;

    image_memory_barrier.newLayout = new_layout;

    /* Must be set to VK_QUEUE_FAMILY_IGNORED if not transfering
     * queue family ownership (not the default value).
     */
    image_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    image_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    image_memory_barrier.image = image;
    image_memory_barrier.subresourceRange.baseMipLevel = 0;
    image_memory_barrier.subresourceRange.levelCount = 1;
    image_memory_barrier.subresourceRange.baseArrayLayer = 0;
    image_memory_barrier.subresourceRange.layerCount = 1;

    if (new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        image_memory_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

        if (this->hasStencilComponent(format)) {
            image_memory_barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    } else {
        image_memory_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    /* The pipeline stages that you are allowed to specify
     * before and after the barrier depend on how you use the
     * resource before and after the barrier. The allowed
     * values are listed in this table of the specification.
     *
     * For example, if you're going to read from a uniform after
     * the barrier, you would specify a usage of
     * VK_ACCESS_UNIFORM_READ_BIT and the earliest shader that
     * will read from the uniform as pipeline stage, for example
     * VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT.
     *
     * https://registry.khronos.org/vulkan/specs/1.3-extensions/html/chap7.html#synchronization-access-types-supported
     * https://registry.khronos.org/vulkan/specs/1.3-extensions/html/chap7.html#VkPipelineStageFlagBits
     */
    VkPipelineStageFlags source_stage {}, destination_stage {};

    if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        image_memory_barrier.srcAccessMask = 0;
        image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        image_memory_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        image_memory_barrier.srcAccessMask = 0;
        image_memory_barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destination_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    } else {
        std::cerr << "[ERROR] Invalid old_layout and new_layout combination." << std::endl;
        return VK_ERROR_VALIDATION_FAILED_EXT;
    }

    vkCmdPipelineBarrier(
        command_buffer,
        source_stage, destination_stage,
        0,
        0, nullptr,
        0, nullptr,
        1, &image_memory_barrier
        );

    VK_CHECK(this->endSingleUseCommandBuffer(command_buffer));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
    VkCommandBuffer command_buffer {};
    VK_CHECK(this->beginSingleUseCommandBuffer(command_buffer));

    VkBufferImageCopy buffer_image_copy {};
    buffer_image_copy.bufferOffset = 0;
    buffer_image_copy.bufferRowLength = 0;
    buffer_image_copy.bufferImageHeight = 0;

    buffer_image_copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    buffer_image_copy.imageSubresource.mipLevel = 0;
    buffer_image_copy.imageSubresource.baseArrayLayer = 0;
    buffer_image_copy.imageSubresource.layerCount = 1;

    buffer_image_copy.imageOffset = {0, 0, 0};
    buffer_image_copy.imageExtent = {width, height, 1};

    vkCmdCopyBufferToImage(
    command_buffer,
        buffer,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &buffer_image_copy
        );

    VK_CHECK(this->endSingleUseCommandBuffer(command_buffer));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createTextureImage() {
    int width, height;
    std::unique_ptr<std::byte[], base::free_deleter> image {nullptr};

    VK_CHECK_R(base::Image::Load("../textures/statue.jpg", width, height, image), VK_ERROR_UNKNOWN);

    std::size_t image_size {static_cast<std::size_t>(width * height * 4)};
    VK_CHECK(this->createBuffer(image_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, this->_staging_buffer, this->_staging_buffer_memory));

    VK_CHECK(this->copyHostToDevice(image.get(), this->_staging_buffer_memory, image_size));

    // Free memory managed by unique_ptr
    image.reset();

    VK_CHECK(this->createImage(width, height, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->_texture_image, this->_texture_image_memory));

    /* Transition the texture image to
     * VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL so the data can be
     * copied to the image.
     */
    VK_CHECK(this->transitionImageLayout(this->_texture_image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL));

    // Execute the buffer to image copy operation
    VK_CHECK(this->copyBufferToImage(this->_staging_buffer, this->_texture_image, static_cast<uint32_t>(width), static_cast<uint32_t>(height)));

    /* One last transition to prepare it for shader access
     * to be able to start sampling from the texture image
     * in the shader.
     */
    VK_CHECK(transitionImageLayout(this->_texture_image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL));

    this->destroyBuffer(this->_staging_buffer, this->_staging_buffer_memory);

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyTextureImage() {
    this->destroyImage(this->_texture_image, this->_texture_image_memory);
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspect_flags, VkImageView& image_view) {
    VkImageViewCreateInfo image_view_create_info {};
    image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create_info.image = image;

    image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create_info.format = format;

    image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    image_view_create_info.subresourceRange.aspectMask = aspect_flags;
    image_view_create_info.subresourceRange.baseMipLevel = 0;
    image_view_create_info.subresourceRange.levelCount = 1;
    image_view_create_info.subresourceRange.baseArrayLayer = 0;
    image_view_create_info.subresourceRange.layerCount = 1;

    VK_CHECK(vkCreateImageView(this->_device, &image_view_create_info, nullptr, &image_view));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyImageView(VkImageView& image_view) {
    vkDestroyImageView(this->_device, image_view, nullptr);
    image_view = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createTextureImageView() {
    VK_CHECK(this->createImageView(this->_texture_image, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT, this->_texture_image_view));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyTextureImageView() {
    this->destroyImageView(this->_texture_image_view);
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createTextureSampler() {
    VkSamplerCreateInfo sampler_create_info {};
    sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_create_info.magFilter = VK_FILTER_LINEAR;
    sampler_create_info.minFilter = VK_FILTER_LINEAR;

    /* VK_SAMPLER_ADDRESS_MODE_REPEAT: Repeat the texture when going beyond the image dimensions.
     * VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT: Like repeat, but inverts the coordinates to mirror the image when going beyond the dimensions.
     * VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE: Take the color of the edge closest to the coordinate beyond the image dimensions.
     * VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE: Like clamp to edge, but instead uses the edge opposite to the closest edge.
     * VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER: Return a solid color when sampling beyond the dimensions of the image.
     */
    sampler_create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

    /* Used for anisotropic filtering. This is an optional feature
     * and must first be enabled in VkPhysicalDeviceFeatures in
     * createLogicalDevice. Make sure to modify isDeviceSuitable to
     * query support.
     *
     * Next, retrieve the max filter level available in the hardware by
     * looking in  VkPhysicalDeviceProperties, looking in the
     * VkPhysicalDeviceLimits member struct called limits, and
     * check the member variable maxSamplerAnisotropy.
     */
    sampler_create_info.anisotropyEnable = VK_FALSE;
    sampler_create_info.maxAnisotropy = 1.0f;

    sampler_create_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    sampler_create_info.unnormalizedCoordinates = VK_FALSE;

    // Used for percentage-closer filtering. Disable for now
    // https://developer.nvidia.com/gpugems/gpugems/part-ii-lighting-and-shadows/chapter-11-shadow-map-antialiasing
    sampler_create_info.compareEnable = VK_FALSE;
    sampler_create_info.compareOp = VK_COMPARE_OP_ALWAYS;

    // Used for mipmapping filters. Save for a later chapter.
    sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler_create_info.mipLodBias = 0.0f;
    sampler_create_info.minLod = 0.0f;
    sampler_create_info.maxLod = 0.0f;

    VK_CHECK(vkCreateSampler(this->_device, &sampler_create_info, nullptr, &this->_texture_sampler));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyTextureSampler() {
    vkDestroySampler(this->_device, this->_texture_sampler, nullptr);
    this->_texture_sampler = VK_NULL_HANDLE;
}

VkFormat
vulkanexamples::examples::VulkanDepthBuffer::findSupportedDepthFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
    for (const VkFormat& format : candidates) {
        VkFormatProperties format_properties {};
        vkGetPhysicalDeviceFormatProperties(this->_physical_device, format, &format_properties);

        if (tiling == VK_IMAGE_TILING_LINEAR &&
            (format_properties.linearTilingFeatures & features) == features) {
            return format;
        } else if (tiling == VK_IMAGE_TILING_OPTIMAL &&
            (format_properties.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    std::cerr << "[Warning] Ideal deph buffer format not found. Default to VK_FORMAT_D32_SFLOAT..." << std::endl;

    return VK_FORMAT_D32_SFLOAT;
}

VkFormat
vulkanexamples::examples::VulkanDepthBuffer::findDepthFormat() {
    return this->findSupportedDepthFormat(
        {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

bool
vulkanexamples::examples::VulkanDepthBuffer::hasStencilComponent(VkFormat format) {
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createSwapChainFramebuffers() {
    this->_swap_chain_framebuffers.resize(this->_swap_chain_images.size());

    for (std::size_t i = 0; i < this->_swap_chain_image_views.size(); ++i) {
        std::array<VkImageView, 2> attachments {
            this->_swap_chain_image_views[i],
            this->_depth_image_view,
            };

        VkFramebufferCreateInfo framebuffer_create_info {};
        framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebuffer_create_info.renderPass = this->_render_pass;
        framebuffer_create_info.attachmentCount = attachments.size();
        framebuffer_create_info.pAttachments = attachments.data();
        framebuffer_create_info.width = this->_swap_chain_details.extent.width;
        framebuffer_create_info.height = this->_swap_chain_details.extent.height;
        framebuffer_create_info.layers = 1;

        VK_CHECK(vkCreateFramebuffer(this->_device, &framebuffer_create_info, nullptr, &this->_swap_chain_framebuffers[i]));
    }

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroySwapChainFramebuffers() {
    for (VkFramebuffer& framebuffer : this->_swap_chain_framebuffers) {
        vkDestroyFramebuffer(this->_device, framebuffer, nullptr);
    }

    this->_swap_chain_framebuffers.clear();
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::recreateSwapChainResources() {
    this->destroySwapChainFramebuffers();
    this->destroyDepthResources();

    VK_CHECK(this->recreateSwapChain());

    VK_CHECK(this->createDepthResources());
    VK_CHECK(this->createSwapChainFramebuffers());

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createDepthResources() {
    VkFormat depth_format = findDepthFormat();

    VK_CHECK(this->createImage(this->_swap_chain_details.extent.width, this->_swap_chain_details.extent.height, depth_format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, this->_depth_image, this->_depth_image_memory));
    VK_CHECK(this->createImageView(this->_depth_image, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT, this->_depth_image_view));

    VK_CHECK(this->transitionImageLayout(this->_depth_image, depth_format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyDepthResources() {
    this->destroyImageView(this->_depth_image_view);
    this->destroyImage(this->_depth_image, this->_depth_image_memory);
}

void
vulkanexamples::examples::VulkanDepthBuffer::updateUniformBuffers() {
    static std::chrono::time_point start_time {
        std::chrono::high_resolution_clock::now()
        };

    std::chrono::time_point current_time {
        std::chrono::high_resolution_clock::now()
        };

    float time = std::chrono::duration<float, std::chrono::seconds::period>(
                    current_time - start_time)
                    .count();

    QuadUBO ubo {};

    ubo.model =
            glm::rotate(glm::mat4{1.0f}, time * glm::radians(90.0f),
                            glm::vec3{0.0f, 0.0f, 1.0f});
    ubo.view =
        glm::lookAt(glm::vec3{2.0f, 2.0f, 2.0f}, glm::vec3{0.0f, 0.0f, 0.0f},
                    glm::vec3{0.0f, 0.0f, 1.0f});
    ubo.proj =
        glm::perspective(glm::radians(45.0f),
                        this->_swap_chain_details.extent.width /
                            (float)this->_swap_chain_details.extent.height,
                        0.1f, 10.0f);
    ubo.proj[1][1] *= -1;

    (void)memcpy(this->_uniform_buffers_mapped_memory[this->_current_frame],
                &ubo, sizeof(ubo));
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createDescriptorPool() {
    std::array<VkDescriptorPoolSize, 2> descriptor_pool_sizes {};
    descriptor_pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_pool_sizes[0].descriptorCount = static_cast<std::uint32_t>(VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT);

    descriptor_pool_sizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptor_pool_sizes[1].descriptorCount = static_cast<std::uint32_t>(VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT);


    VkDescriptorPoolCreateInfo descriptor_pool_create_info {};
    descriptor_pool_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_create_info.poolSizeCount = descriptor_pool_sizes.size();
    descriptor_pool_create_info.pPoolSizes = descriptor_pool_sizes.data();
    descriptor_pool_create_info.maxSets = static_cast<std::uint32_t>(VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT);
    descriptor_pool_create_info.flags = 0; // VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT

    VK_CHECK(vkCreateDescriptorPool(this->_device, &descriptor_pool_create_info, nullptr, &this->_descriptor_pool));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroyDescriptorPool() {
    vkDestroyDescriptorPool(this->_device, this->_descriptor_pool, nullptr);
    this->_descriptor_pool = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createDescriptorSets() {
    std::vector<VkDescriptorSetLayout> descriptor_set_layouts {
        VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT,
        this->_descriptor_set_layout};

    VkDescriptorSetAllocateInfo descriptor_set_allocate_info {};
    descriptor_set_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_set_allocate_info.descriptorPool = this->_descriptor_pool;
    descriptor_set_allocate_info.descriptorSetCount = static_cast<uint32_t>(VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT);
    descriptor_set_allocate_info.pSetLayouts = descriptor_set_layouts.data();

    this->_descriptor_sets.resize(descriptor_set_layouts.size());

    VK_CHECK(vkAllocateDescriptorSets(this->_device, &descriptor_set_allocate_info, this->_descriptor_sets.data()));

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        VkDescriptorBufferInfo buffer_descriptor_info {};
        buffer_descriptor_info.buffer = this->_uniform_buffers[i];
        buffer_descriptor_info.offset = 0;
        buffer_descriptor_info.range = sizeof(QuadUBO);

        VkDescriptorImageInfo image_descriptor_info{};
        image_descriptor_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        image_descriptor_info.imageView = this->_texture_image_view;
        image_descriptor_info.sampler = this->_texture_sampler;

        std::array<VkWriteDescriptorSet, 2> write_descriptor_sets {};
        write_descriptor_sets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write_descriptor_sets[0].dstSet = this->_descriptor_sets[i];
        write_descriptor_sets[0].dstBinding = 0;
        write_descriptor_sets[0].dstArrayElement = 0;
        write_descriptor_sets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        write_descriptor_sets[0].descriptorCount = 1;
        write_descriptor_sets[0].pBufferInfo = &buffer_descriptor_info;
        write_descriptor_sets[0].pImageInfo = nullptr; // Optional
        write_descriptor_sets[0].pTexelBufferView = nullptr; // Optional

        write_descriptor_sets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write_descriptor_sets[1].dstSet = this->_descriptor_sets[i];
        write_descriptor_sets[1].dstBinding = 1;
        write_descriptor_sets[1].dstArrayElement = 0;
        write_descriptor_sets[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        write_descriptor_sets[1].descriptorCount = 1;
        write_descriptor_sets[1].pBufferInfo = nullptr;
        write_descriptor_sets[1].pImageInfo = &image_descriptor_info; // Optional
        write_descriptor_sets[1].pTexelBufferView = nullptr; // Optional

        vkUpdateDescriptorSets(this->_device, write_descriptor_sets.size(), write_descriptor_sets.data(), 0, nullptr);
    }

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createPrimaryCommandBuffers() {

    VkCommandBufferAllocateInfo command_buffer_alloc_info {};
    command_buffer_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_alloc_info.commandPool = this->_command_pool;
    command_buffer_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    command_buffer_alloc_info.commandBufferCount = this->_primary_command_buffers.size();

    VK_CHECK(vkAllocateCommandBuffers(this->_device, &command_buffer_alloc_info, this->_primary_command_buffers.data()));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::populatePrimaryCommandBuffers(uint32_t buffer_index, uint32_t image_index) {
    VK_CHECK(vkResetCommandBuffer(this->_primary_command_buffers[buffer_index], 0));

    VkCommandBufferBeginInfo command_buffer_begin_info {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pNext = nullptr;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VK_CHECK(vkBeginCommandBuffer(this->_primary_command_buffers[buffer_index], &command_buffer_begin_info));

    VkRenderPassBeginInfo render_pass_begin_info {};
    render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_begin_info.renderPass = this->_render_pass;
    render_pass_begin_info.framebuffer = this->_swap_chain_framebuffers[image_index];

    render_pass_begin_info.renderArea.offset = {0, 0};
    render_pass_begin_info.renderArea.extent = this->_swap_chain_details.extent;

    std::array<VkClearValue, 2> clear_values {};
    clear_values[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
    clear_values[1].depthStencil = {1.0f, 0};

    render_pass_begin_info.clearValueCount = clear_values.size();
    render_pass_begin_info.pClearValues = clear_values.data();

    vkCmdBeginRenderPass(this->_primary_command_buffers[buffer_index], &render_pass_begin_info, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

    // Execute render commands from a secondary command buffer
    vkCmdExecuteCommands(this->_primary_command_buffers[buffer_index], 1, &this->_secondary_command_buffers[image_index]);

    // Ending the render pass will add an implicit barrier, transitioning the frame buffer color attachment to
    // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR for presenting it to the windowing system
    vkCmdEndRenderPass(this->_primary_command_buffers[buffer_index]);

    VK_CHECK(vkEndCommandBuffer(this->_primary_command_buffers[buffer_index]))

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createSecondaryCommandBuffers() {
    this->_secondary_command_buffers.resize(this->_swap_chain_framebuffers.size());
    std::fill(this->_secondary_command_buffers.begin(), this->_secondary_command_buffers.end(), VK_NULL_HANDLE);

    VkCommandBufferAllocateInfo command_buffer_alloc_info {};
    command_buffer_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_alloc_info.commandPool = this->_command_pool;
    command_buffer_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    command_buffer_alloc_info.commandBufferCount = this->_secondary_command_buffers.size();

    VK_CHECK(vkAllocateCommandBuffers(this->_device, &command_buffer_alloc_info, this->_secondary_command_buffers.data()));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::populateSecondaryCommandBuffers() {
    // Secondary CBs will be executed by primary CBs inside a (specific subpass of a) render pass instance.
    VkCommandBufferBeginInfo command_buffer_begin_info {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pInheritanceInfo = nullptr; // Optional

    // Explicitly express the intention to inherit the render pass instance state by setting VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT as a flag
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;

    // Set the render pass object defining what render pass instances the SCBs will be compatible with.
    // Specifying the exact framebuffer that the SCBs will be executed with may result in better performance at command buffer execution time.
    VkCommandBufferInheritanceInfo command_buffer_inheritance_info {};
    command_buffer_inheritance_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    command_buffer_inheritance_info.renderPass = this->_render_pass;
    command_buffer_inheritance_info.subpass = 0;

    for (std::size_t i = 0; i < this->_secondary_command_buffers.size(); ++i) {
        // It is technically optional to pair each secondary command buffer to a framebuffer, but
        // the Vulkan specification states that providing the exact framebuffer used by a
        // command buffer yeild better performance.

        /* A framebuffer object is part of the render pass state stored in the
         * command buffer state, which includes several states (pipeline
         * render pass, etc.). However, GPUs have no knowledge of pipelines,
         * render passes, etc. As a result, the Vulkan driver must translate the
         * command buffer state into hardware state. When you specify the
         * framebuffer object for a secondary command buffer, the Vulkan
         * implementation can perform as much pre-processing as possible
         * to translate the command buffer state to the corresponding hardware
         * state during command recording. This avoids repeating this task
         * whenever you call vkCmdExecuteCommands to record a secondary
         * command buffer into a primary one.
         */

        // This means that we could have declared a single secondary command buffer to be
        // used by various primary command buffers within different render pass
        // instances. However, using only one secondary command buffer may prevent the
        // driver from making full optimizations.
        command_buffer_inheritance_info.framebuffer = this->_swap_chain_framebuffers[i];

        // Apply the inheritence information.
        command_buffer_begin_info.pInheritanceInfo = &command_buffer_inheritance_info;

        VK_CHECK(vkBeginCommandBuffer(this->_secondary_command_buffers[i], &command_buffer_begin_info));

        // Commands are not recorded within a subpass of a render pass instance. This information will be inherited
        // from the primary command buffers that will actually execute the secondary command buffers.

        vkCmdBindPipeline(this->_secondary_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->_graphics_pipeline);

        VkViewport viewport {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(this->_swap_chain_details.extent.width);
        viewport.height = static_cast<float>(this->_swap_chain_details.extent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(this->_secondary_command_buffers[i], 0, 1, &viewport);

        VkRect2D scissor {};
        scissor.offset = {0, 0};
        scissor.extent = this->_swap_chain_details.extent;
        vkCmdSetScissor(this->_secondary_command_buffers[i], 0, 1, &scissor);

        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(this->_secondary_command_buffers[i], 0, 1, &this->_vertex_buffer, offsets);
        vkCmdBindIndexBuffer(this->_secondary_command_buffers[i], this->_index_buffer, 0, VK_INDEX_TYPE_UINT16);

        vkCmdBindDescriptorSets(this->_secondary_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->_pipeline_layout, 0, 1, &this->_descriptor_sets[this->_current_frame], 0, nullptr);

        vkCmdDrawIndexed(this->_secondary_command_buffers[i], static_cast<uint32_t>(VulkanDepthBuffer::indices.size()), 1, 0, 0, 0);


        VK_CHECK(vkEndCommandBuffer(this->_secondary_command_buffers[i]));
    }

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::recreateSecondaryCommandBuffers() {
    for (std::size_t i = 0; i < this->_secondary_command_buffers.size(); ++i) {
        VK_CHECK(vkResetCommandBuffer(this->_secondary_command_buffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT));
    }

    VK_CHECK(this->populateSecondaryCommandBuffers());

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::createSyncObjects() {
    VkSemaphoreCreateInfo semaphore_create_info {};
    semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fence_create_info {};
    fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_create_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT; ++i) {
        VK_CHECK(vkCreateSemaphore(this->_device, &semaphore_create_info, nullptr, &this->_image_available_semaphores[i]));

        VK_CHECK(vkCreateSemaphore(this->_device, &semaphore_create_info, nullptr, &this->_render_finished_semaphores[i]));

        VK_CHECK(vkCreateFence(this->_device, &fence_create_info, nullptr, &this->_in_flight_fences[i]));
    }

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanDepthBuffer::destroySyncObjects() {
    for (int i = 0; i < VulkanDepthBuffer::MAX_FRAMES_IN_FLIGHT; ++i) {
        vkDestroySemaphore(this->_device, this->_image_available_semaphores[i], nullptr);
        vkDestroySemaphore(this->_device, this->_render_finished_semaphores[i], nullptr);
        vkDestroyFence(this->_device, this->_in_flight_fences[i], nullptr);
    }

    this->_image_available_semaphores.fill(VK_NULL_HANDLE);
    this->_render_finished_semaphores.fill(VK_NULL_HANDLE);
    this->_in_flight_fences.fill(VK_NULL_HANDLE);
}

VkResult
vulkanexamples::examples::VulkanDepthBuffer::renderFrame() {
    VkResult result {VK_SUCCESS};
    std::uint32_t image_index {0};

    VK_CHECK(vkWaitForFences(this->_device, 1, &this->_in_flight_fences[this->_current_frame], VK_TRUE, std::numeric_limits<std::uint64_t>::max()));

    result = vkAcquireNextImageKHR(this->_device, this->_swap_chain, std::numeric_limits<std::uint64_t>::max(), this->_image_available_semaphores[this->_current_frame], VK_NULL_HANDLE, &image_index);

    // Recreate the swap chain if it is out of date. Do not recreate it if it is
    // suboptimal since we A) already have the image and B) can recreate it later.
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        this->waitEvent();
        VK_CHECK(vkDeviceWaitIdle(this->_device));

        VK_CHECK(this->recreateSwapChainResources());
        VK_CHECK(this->recreateSecondaryCommandBuffers());
        return result;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        std::cerr << "[ERROR] Failed to aquire next swap chain image: " << -result << std::endl;
        return result;
    }

    // Only reset the fence if we are submitting work
    VK_CHECK(vkResetFences(this->_device, 1, &this->_in_flight_fences[this->_current_frame]));

    VK_CHECK(this->populatePrimaryCommandBuffers(this->_current_frame, image_index));

    this->updateUniformBuffers();

    VkSubmitInfo submit_info {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &this->_image_available_semaphores[this->_current_frame];

    VkPipelineStageFlags wait_stage[] {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submit_info.pWaitDstStageMask = wait_stage;

    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &this->_primary_command_buffers[this->_current_frame];

    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &this->_render_finished_semaphores[this->_current_frame];

    VK_CHECK(vkQueueSubmit(this->_graphics_queue, 1, &submit_info, this->_in_flight_fences[this->_current_frame]));

    VkPresentInfoKHR present_info {};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &this->_render_finished_semaphores[this->_current_frame];

    present_info.swapchainCount = 1;
    present_info.pSwapchains = &this->_swap_chain;
    present_info.pImageIndices = &image_index;
    present_info.pResults = nullptr;

    result = vkQueuePresentKHR(this->_present_queue, &present_info);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || this->_framebuffer_resized) {
        this->waitEvent();
        VK_CHECK(vkDeviceWaitIdle(this->_device));

        VK_CHECK(this->recreateSwapChainResources());
        VK_CHECK(this->recreateSecondaryCommandBuffers());
        this->_framebuffer_resized = VK_FALSE;
    } else if (result != VK_SUCCESS) {
        std::cerr << "[ERROR] Failed to present next swap chain image: " << -result << std::endl;
        return result;
    }

    this->_current_frame = (this->_current_frame + 1) & VulkanExampleBase::MAX_FRAMES_IN_FLIGHT;

    return VK_SUCCESS;
}

VK_GENERATE_MAIN(vulkanexamples::examples::VulkanDepthBuffer)
