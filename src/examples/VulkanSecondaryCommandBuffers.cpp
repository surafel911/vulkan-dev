#include <algorithm>
#include <examples/VulkanSecondaryCommandBuffers.hpp>

#include <array>
#include <limits>
#include <cstdint>
#include <cstdlib>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.h>

#include <base/VulkanExampleBase.hpp>
#include <vulkan/vulkan_core.h>

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::setup() {
    this->_current_frame = 0;

    VK_CHECK(this->createPipelineLayout());
    VK_CHECK(this->createRenderPass());
    VK_CHECK(this->createSwapChainFramebuffers());
    VK_CHECK(this->createGraphicsPipeline());
    VK_CHECK(this->createCommandPool());
    VK_CHECK(this->createPrimaryCommandBuffers());
    VK_CHECK(this->createSecondaryCommandBuffers());
    VK_CHECK(this->populateSecondaryCommandBuffers());
    VK_CHECK(this->createSyncObjects());

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::terminate() {
    this->destroySyncObjects();
    this->destroyCommandPool();
    this->destroyGraphicsPipeline();
    this->destroySwapChainFramebuffers();
    this->destroyRenderPass();
    this->destroyPipelineLayout();
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createPipelineLayout() {
    // The pipeline layout specifies any uniform or push constant
    // values in the shaders. Since we are not using either, zero
    // out the structure and create the pipeine layout.
    VkPipelineLayoutCreateInfo pipeline_layout_create_info {};
    pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_create_info.setLayoutCount = 0; // Optional
    pipeline_layout_create_info.pSetLayouts = nullptr; // Optional
    pipeline_layout_create_info.pushConstantRangeCount = 0; // Optional
    pipeline_layout_create_info.pPushConstantRanges = nullptr; // Optional

    VK_CHECK(vkCreatePipelineLayout(this->_device, &pipeline_layout_create_info, nullptr, &this->_pipeline_layout));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::destroyPipelineLayout() {
    vkDestroyPipelineLayout(this->_device, this->_pipeline_layout, nullptr);
    this->_pipeline_layout = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createRenderPass() {
    // We need to tell Vulkan about the framebuffer attachments
    // that will be used while rendering. We need to specify how
    // many color and depth buffers there will be, how many samples
    // to use for each of them and how their contents should be
    // handled throughout the rendering operations. All of this
    // information is wrapped in a render pass object,

    // In this example, the color buffer is the only attachment
    // represented by one of the images from the swap chain.

    VkAttachmentDescription color_attachment_description {};
    color_attachment_description.format = this->_swap_chain_details.format.format;
    
    // Not using multisampling, so use only one sample.
    color_attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;

    // The loadOp and storeOp determine what to do with the data
    // in the attachment before rendering and after rendering. 
    color_attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    color_attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

    // Not using any stencil data, ignore
    color_attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    color_attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    // Do not care about the layout (i.e. pixel format) of a VkImage
    // before rendering, but this does not matter since it is being
    // written over anyways. The image will be used for rendering,
    // so use the present layout.
    color_attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    color_attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    // Subpasses are subsequent rendering operations that depend on the
    // contents of framebuffers in previous passes, for example a sequence
    // of post-processing effects that are applied one after another.
    //
    // In other words, subpasses in a render pass automatically take care
    // of image layout transitions for before, during, and after rendering.
    // 
    // In this example, we will only use one subpass for the main render pass.

    // Each subpass references one or more attachments, which is defined below.
    VkAttachmentReference color_attachment_reference {};
    // This is the index into the attachment description array
    // this reference applies to.
    color_attachment_reference.attachment = 0;
    color_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass_description {};
    subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

    // The index of the attachment in this array is directly referenced from
    // the fragment shader with the layout(location = 0) out vec4 outColor
    // directive.
    subpass_description.colorAttachmentCount = 1;
    subpass_description.pColorAttachments = &color_attachment_reference;

    // The image layout transisions are c ontrolled by the subpass 
    // dependency. They tell the render pass how to handle an image
    // transition. The render pass is the only subpass we define, but the
    // transition before and after the render pass count as "implicit"
    // subpasses.
    //
    // The problem is that the starting subpass will transition the images
    // before we have aquired a swap chain image, therefore we will explicitly
    // create a subpass dependency to block the render pass until the image is
    // available

    VkSubpassDependency subpass_dependency {};

    // The special value VK_SUBPASS_EXTERNAL refers to the implicit subpass
    // before or after the render pass depending on whether it is specified
    // in srcSubpass or dstSubpass.
    subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;

    // The index 0 refers to our subpass, which is the first and only one.
    subpass_dependency.dstSubpass = 0;

    // specify the operations to wait on and the stages in which these
    // operations occur. We need to wait for the swap chain to finish reading
    // from the image before we can access it. This can be accomplished by waiting on the color attachment output stage 
    subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    subpass_dependency.srcAccessMask = 0;

    // The operations that should wait on this are in the color attachment
    // stage and involve the writing of the color attachment. These settings
    // will prevent the transition from happening until it's actually necessary
    subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkRenderPassCreateInfo renderPassInfo {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &color_attachment_description;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass_description;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &subpass_dependency;

    VK_CHECK(vkCreateRenderPass(this->_device, &renderPassInfo, nullptr, &this->_render_pass));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::destroyRenderPass() {
    vkDestroyRenderPass(this->_device, this->_render_pass, nullptr);
    this->_render_pass = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createSwapChainFramebuffers() {
    this->_swap_chain_framebuffers.resize(this->_swap_chain_images.size());
    
    for (std::size_t i = 0; i < this->_swap_chain_image_views.size(); ++i) {
        std::array<VkImageView, 1> attachments {this->_swap_chain_image_views[i]};

        VkFramebufferCreateInfo framebuffer_create_info {};
        framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebuffer_create_info.renderPass = this->_render_pass;
        framebuffer_create_info.attachmentCount = attachments.size();
        framebuffer_create_info.pAttachments = attachments.data();
        framebuffer_create_info.width = this->_swap_chain_details.extent.width;
        framebuffer_create_info.height = this->_swap_chain_details.extent.height;
        framebuffer_create_info.layers = 1;

        VK_CHECK(vkCreateFramebuffer(this->_device, &framebuffer_create_info, nullptr, &this->_swap_chain_framebuffers[i]));
    }

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::destroySwapChainFramebuffers() {
    for (VkFramebuffer& framebuffer : this->_swap_chain_framebuffers) {
        vkDestroyFramebuffer(this->_device, framebuffer, nullptr);
    }

    this->_swap_chain_framebuffers.clear();
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::recreateSwapChainResources() {
    this->destroySwapChainFramebuffers();
    
    VK_CHECK(this->recreateSwapChain());
    
    VK_CHECK(this->createSwapChainFramebuffers());

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createGraphicsPipeline() {
    std::array dynamic_states {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
        };

    VkPipelineDynamicStateCreateInfo dynamic_state_create_info {};
    dynamic_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state_create_info.dynamicStateCount = static_cast<uint32_t>(dynamic_states.size());
    dynamic_state_create_info.pDynamicStates = dynamic_states.data();

    std::vector<std::byte> vertex_buffer {};
    VK_CHECK_R(readFile("shaders/triangle/triangle.spirv.vert", vertex_buffer), VK_ERROR_INVALID_SHADER_NV);

    std::vector<std::byte> fragment_buffer {};
    VK_CHECK_R(readFile("shaders/triangle/triangle.spirv.frag", fragment_buffer), VK_ERROR_INVALID_SHADER_NV);


    VkShaderModule vertex_shader_module {};
    VK_CHECK(this->createShaderModule(vertex_buffer, vertex_shader_module));


    VkShaderModule fragment_shader_module {};
    VK_CHECK(this->createShaderModule(fragment_buffer, fragment_shader_module));

    VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info {};
    vertex_input_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertex_input_state_create_info.vertexBindingDescriptionCount = 0;
    vertex_input_state_create_info.pVertexBindingDescriptions = nullptr; // Optional
    vertex_input_state_create_info.vertexAttributeDescriptionCount = 0;
    vertex_input_state_create_info.pVertexAttributeDescriptions = nullptr; // Optional

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info {};
    input_assembly_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    input_assembly_state_create_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    input_assembly_state_create_info.primitiveRestartEnable = VK_FALSE;

    std::array<VkPipelineShaderStageCreateInfo, 2> shader_stages_create_info {};

    shader_stages_create_info[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shader_stages_create_info[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    shader_stages_create_info[0].module = vertex_shader_module;
    shader_stages_create_info[0].pName = "main";

    shader_stages_create_info[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shader_stages_create_info[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    shader_stages_create_info[1].module = fragment_shader_module;
    shader_stages_create_info[1].pName = "main";

    VkPipelineViewportStateCreateInfo viewport_state_create_info {};
    viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state_create_info.viewportCount = 1;
    viewport_state_create_info.scissorCount = 1;

    VkPipelineRasterizationStateCreateInfo rasterizer_state_create_info {};
    rasterizer_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer_state_create_info.depthClampEnable = VK_FALSE;
    rasterizer_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    rasterizer_state_create_info.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer_state_create_info.lineWidth = 1.0f;
    rasterizer_state_create_info.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer_state_create_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizer_state_create_info.depthBiasEnable = VK_FALSE;
    rasterizer_state_create_info.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer_state_create_info.depthBiasClamp = 0.0f; // Optional
    rasterizer_state_create_info.depthBiasSlopeFactor = 0.0f; // Optional

    VkPipelineMultisampleStateCreateInfo multisampling_state_create_info{};
    multisampling_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling_state_create_info.sampleShadingEnable = VK_FALSE;
    multisampling_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling_state_create_info.minSampleShading = 1.0f; // Optional
    multisampling_state_create_info.pSampleMask = nullptr; // Optional
    multisampling_state_create_info.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling_state_create_info.alphaToOneEnable = VK_FALSE; // Optional

    /* Configuration per-attached framebuffer. There is only one framebuffer
     * so we only have one attachment state struct.
     *
     * This configuration will discard the color in the framebuffer with the
     * color produced from the fragment shader.
     */
    VkPipelineColorBlendAttachmentState color_blend_attachment_state {};
    color_blend_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    color_blend_attachment_state.blendEnable = VK_FALSE;
    color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    /* Contains the global color blending settings.
     * 
     * References the array of blend attachment states for all of the framebuffers
     * and sets blend constants that for use as blend factors in the
     * blending calculations.
     */
    VkPipelineColorBlendStateCreateInfo color_blending_state_create_info {};
    color_blending_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blending_state_create_info.logicOpEnable = VK_FALSE;
    color_blending_state_create_info.logicOp = VK_LOGIC_OP_COPY; // Optional
    color_blending_state_create_info.attachmentCount = 1;
    color_blending_state_create_info.pAttachments = &color_blend_attachment_state;
    color_blending_state_create_info.blendConstants[0] = 0.0f; // Optional
    color_blending_state_create_info.blendConstants[1] = 0.0f; // Optional
    color_blending_state_create_info.blendConstants[2] = 0.0f; // Optional
    color_blending_state_create_info.blendConstants[3] = 0.0f; // Optional

    VkGraphicsPipelineCreateInfo pipelineInfo {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = shader_stages_create_info.size();
    pipelineInfo.pStages = shader_stages_create_info.data();

    pipelineInfo.pVertexInputState = &vertex_input_state_create_info;
    pipelineInfo.pInputAssemblyState = &input_assembly_state_create_info;
    pipelineInfo.pViewportState = &viewport_state_create_info;
    pipelineInfo.pRasterizationState = &rasterizer_state_create_info;
    pipelineInfo.pMultisampleState = &multisampling_state_create_info;
    pipelineInfo.pDepthStencilState = nullptr; // Optional
    pipelineInfo.pColorBlendState = &color_blending_state_create_info;
    pipelineInfo.pDynamicState = &dynamic_state_create_info;

    pipelineInfo.layout = this->_pipeline_layout;

    pipelineInfo.renderPass = this->_render_pass;
    pipelineInfo.subpass = 0;

    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    VK_CHECK(vkCreateGraphicsPipelines(this->_device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &this->_graphics_pipeline));

    vkDestroyShaderModule(this->_device, fragment_shader_module, nullptr);
    
    vkDestroyShaderModule(this->_device, vertex_shader_module, nullptr);
    

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::destroyGraphicsPipeline() {
    vkDestroyPipeline(this->_device, this->_graphics_pipeline, nullptr);
    this->_graphics_pipeline = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createCommandPool() {
    VkCommandPoolCreateInfo command_pool_create_info {};
    command_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_pool_create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    command_pool_create_info.queueFamilyIndex = this->_queue_family_indices.graphics_queue_family.value();

    VK_CHECK(vkCreateCommandPool(this->_device, &command_pool_create_info, nullptr, &this->_command_pool));

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::destroyCommandPool() {
    vkDestroyCommandPool(this->_device, this->_command_pool, nullptr);
    this->_command_pool = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createPrimaryCommandBuffers() {
    VkCommandBufferAllocateInfo command_buffer_alloc_info {};
    command_buffer_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_alloc_info.commandPool = this->_command_pool;
    command_buffer_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    command_buffer_alloc_info.commandBufferCount = this->_primary_command_buffers.size();

    VK_CHECK(vkAllocateCommandBuffers(this->_device, &command_buffer_alloc_info, this->_primary_command_buffers.data()));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::populatePrimaryCommandBuffers(uint32_t buffer_index, uint32_t image_index) {
    VK_CHECK(vkResetCommandBuffer(this->_primary_command_buffers[buffer_index], 0));

    VkCommandBufferBeginInfo command_buffer_begin_info {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pNext = nullptr;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    
    VK_CHECK(vkBeginCommandBuffer(this->_primary_command_buffers[buffer_index], &command_buffer_begin_info));

    VkRenderPassBeginInfo render_pass_begin_info {};
    render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_begin_info.renderPass = this->_render_pass;
    render_pass_begin_info.framebuffer = this->_swap_chain_framebuffers[image_index];

    render_pass_begin_info.renderArea.offset = {0, 0};
    render_pass_begin_info.renderArea.extent = this->_swap_chain_details.extent;

    VkClearValue clear_color {{{0.0f, 0.0f, 0.0f, 1.0f}}};
    render_pass_begin_info.clearValueCount = 1;
    render_pass_begin_info.pClearValues = &clear_color;

    vkCmdBeginRenderPass(this->_primary_command_buffers[buffer_index], &render_pass_begin_info, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

    // Execute render commands from a secondary command buffer
    vkCmdExecuteCommands(this->_primary_command_buffers[buffer_index], 1, &this->_secondary_command_buffers[image_index]);

    // Ending the render pass will add an implicit barrier, transitioning the frame buffer color attachment to
    // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR for presenting it to the windowing system
    vkCmdEndRenderPass(this->_primary_command_buffers[buffer_index]);
    
    VK_CHECK(vkEndCommandBuffer(this->_primary_command_buffers[buffer_index]))

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createSecondaryCommandBuffers() {
    this->_secondary_command_buffers.resize(this->_swap_chain_framebuffers.size());
    std::fill(this->_secondary_command_buffers.begin(), this->_secondary_command_buffers.end(), VK_NULL_HANDLE);

    VkCommandBufferAllocateInfo command_buffer_alloc_info {};
    command_buffer_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_alloc_info.commandPool = this->_command_pool;
    command_buffer_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    command_buffer_alloc_info.commandBufferCount = this->_secondary_command_buffers.size();

    VK_CHECK(vkAllocateCommandBuffers(this->_device, &command_buffer_alloc_info, this->_secondary_command_buffers.data()));

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::populateSecondaryCommandBuffers() {
    // Secondary CBs will be executed by primary CBs inside a (specific subpass of a) render pass instance.
    VkCommandBufferBeginInfo command_buffer_begin_info {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pInheritanceInfo = nullptr; // Optional

    // Explicitly express the intention to inherit the render pass instance state by setting VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT as a flag
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;

    // Set the render pass object defining what render pass instances the SCBs will be compatible with.
    // Specifying the exact framebuffer that the SCBs will be executed with may result in better performance at command buffer execution time.
    VkCommandBufferInheritanceInfo command_buffer_inheritance_info {};
    command_buffer_inheritance_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    command_buffer_inheritance_info.renderPass = this->_render_pass;
    command_buffer_inheritance_info.subpass = 0;

    for (std::size_t i = 0; i < this->_secondary_command_buffers.size(); ++i) {
        // It is technically optional to pair each secondary command buffer to a framebuffer, but
        // the Vulkan specification states that providing the exact framebuffer used by a
        // command buffer yeild better performance.

        /* A framebuffer object is part of the render pass state stored in the
         * command buffer state, which includes several states (pipeline
         * render pass, etc.). However, GPUs have no knowledge of pipelines,
         * render passes, etc. As a result, the Vulkan driver must translate the
         * command buffer state into hardware state. When you specify the
         * framebuffer object for a secondary command buffer, the Vulkan
         * implementation can perform as much pre-processing as possible
         * to translate the command buffer state to the corresponding hardware
         * state during command recording. This avoids repeating this task
         * whenever you call vkCmdExecuteCommands to record a secondary
         * command buffer into a primary one.
         */

        // This means that we could have declared a single secondary command buffer to be
        // used by various primary command buffers within different render pass
        // instances. However, using only one secondary command buffer may prevent the
        // driver from making full optimizations.
        command_buffer_inheritance_info.framebuffer = this->_swap_chain_framebuffers[i];
        
        // Apply the inheritence information.
        command_buffer_begin_info.pInheritanceInfo = &command_buffer_inheritance_info;

        VK_CHECK(vkBeginCommandBuffer(this->_secondary_command_buffers[i], &command_buffer_begin_info));

        // Commands are not recorded within a subpass of a render pass instance. This information will be inherited
        // from the primary command buffers that will actually execute the secondary command buffers.

        vkCmdBindPipeline(this->_secondary_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->_graphics_pipeline);

        VkViewport viewport {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(this->_swap_chain_details.extent.width);
        viewport.height = static_cast<float>(this->_swap_chain_details.extent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        vkCmdSetViewport(this->_secondary_command_buffers[i], 0, 1, &viewport);

        VkRect2D scissor {};
        scissor.offset = {0, 0};
        scissor.extent = this->_swap_chain_details.extent;
        vkCmdSetScissor(this->_secondary_command_buffers[i], 0, 1, &scissor);

        vkCmdDraw(this->_secondary_command_buffers[i], 3, 1, 0, 0);

        VK_CHECK(vkEndCommandBuffer(this->_secondary_command_buffers[i]));
    }

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::recreateSecondaryCommandBuffers() {
    for (std::size_t i = 0; i < this->_secondary_command_buffers.size(); ++i) {
        VK_CHECK(vkResetCommandBuffer(this->_secondary_command_buffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT));
    }

    VK_CHECK(this->populateSecondaryCommandBuffers());

    return VK_SUCCESS;
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::createSyncObjects() {
    VkSemaphoreCreateInfo semaphore_create_info {};
    semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fence_create_info {};
    fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_create_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < base::VulkanExampleBase::MAX_FRAMES_IN_FLIGHT; ++i) {
        VK_CHECK(vkCreateSemaphore(this->_device, &semaphore_create_info, nullptr, &this->_image_available_semaphores[i]));

        VK_CHECK(vkCreateSemaphore(this->_device, &semaphore_create_info, nullptr, &this->_render_finished_semaphores[i]));

        VK_CHECK(vkCreateFence(this->_device, &fence_create_info, nullptr, &this->_in_flight_fences[i]));
    }

    return VK_SUCCESS;
}

void
vulkanexamples::examples::VulkanSecondCommandBuffer::destroySyncObjects() {
    for (int i = 0; i < base::VulkanExampleBase::MAX_FRAMES_IN_FLIGHT; ++i) {
        vkDestroySemaphore(this->_device, this->_image_available_semaphores[i], nullptr);
        vkDestroySemaphore(this->_device, this->_render_finished_semaphores[i], nullptr);
        vkDestroyFence(this->_device, this->_in_flight_fences[i], nullptr);
    }

    this->_image_available_semaphores.fill(VK_NULL_HANDLE);
    this->_render_finished_semaphores.fill(VK_NULL_HANDLE);
    this->_in_flight_fences.fill(VK_NULL_HANDLE);
}

VkResult
vulkanexamples::examples::VulkanSecondCommandBuffer::renderFrame() {
    VkResult result {VK_SUCCESS};
    std::uint32_t image_index {0};

    VK_CHECK(vkWaitForFences(this->_device, 1, &this->_in_flight_fences[this->_current_frame], VK_TRUE, std::numeric_limits<std::uint64_t>::max()));

    result = vkAcquireNextImageKHR(this->_device, this->_swap_chain, std::numeric_limits<std::uint64_t>::max(), this->_image_available_semaphores[this->_current_frame], VK_NULL_HANDLE, &image_index);

    // Recreate the swap chain if it is out of date. Do not recreate it if it is
    // suboptimal since we A) already have the image and B) can recreate it later.
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        this->waitEvent();
        VK_CHECK(vkDeviceWaitIdle(this->_device));

        VK_CHECK(this->recreateSwapChainResources());
        VK_CHECK(this->recreateSecondaryCommandBuffers());
        return result;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        std::cerr << "[ERROR] Failed to aquire next swap chain image: " << -result << std::endl;
        return result;
    }

    // Only reset the fence if we are submitting work
    VK_CHECK(vkResetFences(this->_device, 1, &this->_in_flight_fences[this->_current_frame]));

    VK_CHECK(this->populatePrimaryCommandBuffers(this->_current_frame, image_index));

    VkSubmitInfo submit_info {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &this->_image_available_semaphores[this->_current_frame];

    VkPipelineStageFlags wait_stage[] {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submit_info.pWaitDstStageMask = wait_stage;

    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &this->_primary_command_buffers[this->_current_frame];

    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &this->_render_finished_semaphores[this->_current_frame];

    VK_CHECK(vkQueueSubmit(this->_graphics_queue, 1, &submit_info, this->_in_flight_fences[this->_current_frame]));

    VkPresentInfoKHR present_info {};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &this->_render_finished_semaphores[this->_current_frame];

    present_info.swapchainCount = 1;
    present_info.pSwapchains = &this->_swap_chain;
    present_info.pImageIndices = &image_index;
    present_info.pResults = nullptr;

    result = vkQueuePresentKHR(this->_present_queue, &present_info);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || this->_framebuffer_resized) {
        this->waitEvent();
        VK_CHECK(vkDeviceWaitIdle(this->_device));

        VK_CHECK(this->recreateSwapChainResources());
        VK_CHECK(this->recreateSecondaryCommandBuffers());
        this->_framebuffer_resized = VK_FALSE;
    } else if (result != VK_SUCCESS) {
        std::cerr << "[ERROR] Failed to present next swap chain image: " << -result << std::endl;
        return result;
    }

    this->_current_frame = (this->_current_frame + 1) & VulkanExampleBase::MAX_FRAMES_IN_FLIGHT;

    return VK_SUCCESS;
}

VK_GENERATE_MAIN(vulkanexamples::examples::VulkanSecondCommandBuffer)