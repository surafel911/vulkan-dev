#include <base/Mesh.hpp>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

int
vulkanexamples::base::Mesh::LoadObj(std::string_view path, Mesh& mesh) {
    std::string warn {}, err {};

    if (!tinyobj::LoadObj(&mesh._attributes, &mesh._shapes, &mesh._materials, &warn, &err, path.data())) {
        if (!err.empty()) {
            std::cerr << "[TINYOBJ] [ERROR] " << err << std::endl;
        }

        if (!warn.empty()) {
            std::cerr << "[TINYOBJ] [WARN] " << warn << std::endl;
        }
        
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}