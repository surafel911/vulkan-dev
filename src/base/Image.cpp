#include <base/Image.hpp>

#include <memory>
#include <cstdlib>
#include <iostream>
#include <filesystem>
#include <string_view>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

int
vulkanexamples::base::Image::Load(std::string_view path, int& x, int& y, std::unique_ptr<std::byte[], free_deleter>& image) {
    int n;
    unsigned char* data {stbi_load(path.data(), &x, &y, &n, STBI_rgb_alpha)};

    if (!data) {
        std::cerr << "[Error] Failed to load image file: " << path << " working dir: " << std::filesystem::current_path() << std::endl;
        return -EXIT_FAILURE;
    }

    image = std::unique_ptr<std::byte[], free_deleter>(reinterpret_cast<std::byte*>(data));

    return EXIT_SUCCESS;
}
