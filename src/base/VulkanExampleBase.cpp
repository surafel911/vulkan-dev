#include <base/VulkanExampleBase.hpp>

#include <unistd.h>
#include <getopt.h>

#include <set>
#include <array>
#include <chrono>
#include <vector>
#include <limits>
#include <fstream>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <optional>
#include <algorithm>
#include <filesystem>
#include <string_view>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.h>

constexpr uint32_t VULKAN_SWAP_CHAIN_IMAGE_ARRAY_LAYERS_2D = 1;

namespace {
    static VKAPI_ATTR VkBool32
    VKAPI_CALL _debug_messenger_callback(
        VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData
    ) {
        (void)messageType;
        (void)pUserData;

        std::cerr << "[Validation Layer] ";

        switch (message_severity) {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                std::cerr << "[VERBOSE]\t";
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                std::cerr << "[INFO]\t";
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                std::cerr << "[WARNING]\t";
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                std::cerr << "[ERROR]\t";
                break;
            default:
                std::cerr << "[UNKNOWN]\t";
                break;
        }

        std::cerr << pCallbackData->pMessage << std::endl;

        return VK_FALSE;
    }
}

void
vulkanexamples::base::VulkanExampleBase::Run(const int argc, char* const argv[]) {
    SDL_Event event {};
    VkResult result {VK_SUCCESS};
    std::chrono::high_resolution_clock::time_point start_time {};

    static int usage {false}, wayland {true}, no_validation {false};

    static struct option options[] = {
        { "help", no_argument, &usage, true },
        { "x11", no_argument, &wayland, false },
        { "wayland", no_argument, &wayland, true },
        { "no-validation", no_argument, &no_validation, true },
        { nullptr, 0, nullptr, 0 }
        };

    int opt = 0, opt_index = 0;
    while((opt = getopt_long(argc, argv, ":w:h", options, &opt_index)) != -1) {
        switch (opt) {
            case 'w':
                std::filesystem::current_path(optarg);
                break;
            case 'h':
            case '?':
                usage = true;
                break;
            default:
                break;
        }
    }

    if (usage) {
        printf("usage\n");
        printf("\t-w\t\t\tChanges working directory \n");
        printf("\t-h\t\t\tPrints this help screen\n");
        printf("\t --help\n");
        printf("\t--x11\t\t\tUse X11 SDL video driver\n");
        printf("\t--wayland\t\tUse Wayland SDL video driver (default)\n");
        printf("\t--no-validation\t\t\tDisable validation layers\n");
        return;
    }

    if (wayland) {
        std::cout << "Using Wayland SDL video driver." << std::endl;
        SDL_SetHintWithPriority(SDL_HINT_VIDEODRIVER, "wayland", SDL_HINT_OVERRIDE);
    } else {
        std::cout << "Using X11 SDL video driver." << std::endl;
        SDL_SetHintWithPriority(SDL_HINT_VIDEODRIVER, "x11", SDL_HINT_OVERRIDE);
    }

    this->_settings.enable_validation = !no_validation;

    if (int result = this->initSDL(); result != 0) {
        std::cerr << "[Error] Error on SDL init " << -result << std::endl;
        goto EXIT;
    }

    if (result = this->initVulkan(); result != VK_SUCCESS) {
        std::cerr << "[Error] Error on Vulkan base init " << -result << std::endl;
        goto EXIT;
    }

    if (result = this->setup(); result != VK_SUCCESS) {
        std::cerr << "[Error] Error on Vulkan example setup " << -result << std::endl;
        goto EXIT;
    }

    start_time = std::chrono::high_resolution_clock::now();

    while (!this->_quit) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                this->_quit = true;
            }

            if (this->handleEvent(event) != SDL_TRUE) {
                this->_quit = true;
            }
        }

        static std::chrono::high_resolution_clock::time_point current_time {};
        current_time = std::chrono::high_resolution_clock::now();

        this->_delta_time =
            std::chrono::duration<double, std::chrono::milliseconds::period>(current_time - start_time).count();

        if (result = this->renderFrame(); result != VK_SUCCESS && result != VK_ERROR_OUT_OF_DATE_KHR && result != VK_SUBOPTIMAL_KHR) {
            goto EXIT;
        }

        start_time = std::chrono::high_resolution_clock::now();
    }

    EXIT:

    (void)vkDeviceWaitIdle(this->_device);

    this->terminate();
    this->quitVulkan();
    this->quitSDL();
}

SDL_bool
vulkanexamples::base::VulkanExampleBase::handleEvent(const SDL_Event& event) {
    if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED) {
        this->_framebuffer_resized = VK_TRUE;
    }

    if (event.type == SDL_KEYDOWN && (event.key.keysym.scancode == SDL_SCANCODE_RETURN || event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)) {
        this->_quit = true;
    }

    return SDL_TRUE;
}

void
vulkanexamples::base::VulkanExampleBase::waitEvent() {
    if (SDL_GetWindowFlags(this->_window) & SDL_WINDOW_MINIMIZED) {
        SDL_Event event {};
        (void)SDL_WaitEvent(&event);
    }
}

int
vulkanexamples::base::VulkanExampleBase::readFile(std::string_view path, std::vector<std::byte>& buffer) {
    std::ifstream file {path.data(), std::ios::ate | std::ios::binary};

    if (!file.is_open()) {
        std::cerr << "[Error] Failed to open file: " << path << " working dir: " << std::filesystem::current_path() << std::endl;
        return -EXIT_FAILURE;
    }

    std::size_t size (file.tellg());
    buffer.resize(size);

    file.seekg(0);
    file.read(reinterpret_cast<char*>(buffer.data()), size);

    file.close();

    return EXIT_SUCCESS;
}

int
vulkanexamples::base::VulkanExampleBase::initSDL() {
    if (int ret = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS); ret < 0) {
        std::cerr << "[Error] Failed to initialize SDL: " << SDL_GetError() << std::endl;
        return ret;
    }

    this->_window = SDL_CreateWindow(
                     this->_settings.title.data(),
                     this->_settings.window_size.x,
                     this->_settings.window_size.y,
                     this->_settings.window_size.w,
                     this->_settings.window_size.h,
                     SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_VULKAN
                     );
    if (!this->_window) {
        std::cerr << "[Error] Failed to create SDL window: " << SDL_GetError() << std::endl;
        return -EXIT_FAILURE;
    }

    std::uint32_t instance_extension_count {0};
    if (!SDL_Vulkan_GetInstanceExtensions(this->_window, &instance_extension_count, nullptr)) {
        std::cerr << "[Error] Failed to get SDL Vulkan extension count: " << SDL_GetError() << "\n";
        return -EXIT_FAILURE;
    }

    this->_enabled_instance_extensions.resize(instance_extension_count);
    if (!SDL_Vulkan_GetInstanceExtensions(this->_window, &instance_extension_count, this->_enabled_instance_extensions.data())) {
        std::cerr << "[Error] Failed to get SDL Vulkan extensions: " << SDL_GetError() << "\n";
        return -EXIT_FAILURE;
    }

    this->_enabled_instance_extensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    if (this->_settings.enable_validation) {
        this->_validation_layers.emplace_back("VK_LAYER_KHRONOS_validation");
    }

    this->_enabled_device_extensions.emplace_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

    return 0;
}

void
vulkanexamples::base::VulkanExampleBase::quitSDL() {
    SDL_DestroyWindow(this->_window);

    SDL_Quit();
}

VkResult
vulkanexamples::base::VulkanExampleBase::initVulkan() {

    if (this->_settings.enable_validation) {
        VkDebugUtilsMessengerCreateInfoEXT debug_messenger_create_info {};
        debug_messenger_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        debug_messenger_create_info.pNext = nullptr;
        debug_messenger_create_info.flags = VkDebugUtilsMessengerCreateFlagsEXT{};
        debug_messenger_create_info.pfnUserCallback = _debug_messenger_callback;
        debug_messenger_create_info.pUserData = nullptr;

        debug_messenger_create_info.messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

        debug_messenger_create_info.messageSeverity =
            // VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
            // VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
            // VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        VK_CHECK(this->createInstance(&debug_messenger_create_info));
        VK_CHECK(this->createDebugMessenger(&debug_messenger_create_info));
    } else {
        VK_CHECK(this->createInstance(nullptr));
    }
    VK_CHECK(this->createSurface());
    VK_CHECK(this->pickPhysicalDevice());
    VK_CHECK(this->createLogicalDevice());

    this->getGraphicsQueue();
    this->getPresentQueue();

    VK_CHECK(this->createSwapChain());
    VK_CHECK(this->createSwapChainImages());
    VK_CHECK(this->createSwapChainImageViews());

    this->_framebuffer_resized = VK_FALSE;

    this->_quit = false;

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::quitVulkan() {
    (void)vkDeviceWaitIdle(this->_device);

    this->destroySwapChainImageViews();
    this->destroySwapChainImages();
    this->destroySwapChain();
    this->destroyLogicalDevice();
    this->destroySurface();

    if (this->_settings.enable_validation) {
        this->destroyDebugMessenger();
    }

    this->destroyInstance();
}

VkResult
vulkanexamples::base::VulkanExampleBase::createInstance(VkDebugUtilsMessengerCreateInfoEXT* debug_messenger_create_info) {
    VkApplicationInfo app_info {
        VK_STRUCTURE_TYPE_APPLICATION_INFO,
        nullptr,
        "Vulkan",
        VK_MAKE_API_VERSION(0, 0, 0, 1),
        "Abacus",
        VK_MAKE_API_VERSION(0, 0, 0, 1),
        VK_API_VERSION_1_3
        };

    VkInstanceCreateInfo instance_create_info {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        debug_messenger_create_info,
        VkInstanceCreateFlags{},
        &app_info,
        static_cast<std::uint32_t>(this->_validation_layers.size()),
        this->_validation_layers.data(),
        static_cast<std::uint32_t>(this->_enabled_instance_extensions.size()),
        this->_enabled_instance_extensions.data()
        };

    VK_CHECK(vkCreateInstance(&instance_create_info, nullptr, &this->_instance));

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroyInstance() {
    vkDestroyInstance(this->_instance, nullptr);
    this->_instance = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::base::VulkanExampleBase::createDebugMessenger(VkDebugUtilsMessengerCreateInfoEXT* debug_messenger_create_info) {
    this->_vkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(this->_instance, "vkCreateDebugUtilsMessengerEXT"));
    if (this->_vkCreateDebugUtilsMessengerEXT == nullptr) {
        std::cerr << "Failed to get address for vkCreateDebugUtilsMessengerEXT" << std::endl;
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }

    this->_vkDestroyDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(this->_instance, "vkDestroyDebugUtilsMessengerEXT"));
    if (this->_vkDestroyDebugUtilsMessengerEXT == nullptr) {
        std::cerr << "Failed to get address for vkDestroyDebugUtilsMessengerEXT" << std::endl;
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }

    VK_CHECK(this->_vkCreateDebugUtilsMessengerEXT(this->_instance, debug_messenger_create_info, nullptr, &this->_debug_messenger));

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroyDebugMessenger() {
    this->_vkDestroyDebugUtilsMessengerEXT(this->_instance, this->_debug_messenger, nullptr);
    this->_debug_messenger = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::base::VulkanExampleBase::createSurface() {
    if (!SDL_Vulkan_CreateSurface(this->_window, this->_instance, &this->_surface)) {
        std::cerr << "[Error] Failed to create SDL window: " << SDL_GetError() << std::endl;
        return VK_ERROR_INITIALIZATION_FAILED;
    }

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroySurface() {
    vkDestroySurfaceKHR(this->_instance, this->_surface, nullptr);
    this->_surface = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::base::VulkanExampleBase::pickPhysicalDevice() {
    this->_physical_device = VK_NULL_HANDLE;

    std::uint32_t physical_device_count {0};
    VK_CHECK(vkEnumeratePhysicalDevices(this->_instance, &physical_device_count, nullptr));

    if (physical_device_count == 0) {
        std::cerr << "[Error] No Vulkan capable physical devices available" << std::endl;
        return VK_ERROR_INITIALIZATION_FAILED;
    }

    std::vector<VkPhysicalDevice> physical_devices {physical_device_count};
    VK_CHECK(vkEnumeratePhysicalDevices(this->_instance, &physical_device_count, physical_devices.data()));

    for (VkPhysicalDevice& physical_device : physical_devices) {
        if (this->isPhysicalDeviceSuitable(physical_device)) {
            this->_physical_device = physical_device;
            break;
        }
    }

    if (this->_physical_device == VK_NULL_HANDLE) {
        std::cerr << "[Error] Failed to find suitable physical device" << std::endl;
        return VK_ERROR_INITIALIZATION_FAILED;
    }

    return VK_SUCCESS;
}

VkBool32
vulkanexamples::base::VulkanExampleBase::isPhysicalDeviceSuitable(const VkPhysicalDevice& physical_device) {
    vkGetPhysicalDeviceProperties(physical_device, &this->_physical_device_properties);

    vkGetPhysicalDeviceFeatures(physical_device, &this->_physical_device_features);

    switch (this->_physical_device_properties.deviceType) {
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
            break;
        default:
            return VK_FALSE;
    }

    // if (!this->_physical_device_features.geometryShader
    //     && !this->_physical_device_features.multiViewport
    //     && !this->_physical_device_features.multiDrawIndirect) {
    //     return VK_FALSE;
    // }

    this->findQueueFamilies(physical_device);
    if (!this->_queue_family_indices.IsComplete()) {
        std::cerr << "[ERROR] No suitable queue families" << std::endl;
        return VK_FALSE;
    }

    std::vector<VkExtensionProperties> device_extensions {};
    VK_CHECK(this->queryDeviceExtensions(physical_device, device_extensions));

    if (!this->checkDeviceExtensionSupport(device_extensions)) {
        std::cerr << "[EROR] Does not support device extensions" << std::endl;
        return VK_FALSE;
    }

    SwapChainSupportDetails swap_chain_support_details {};
    VK_CHECK(this->querySwapChainDetails(physical_device, swap_chain_support_details));

    if (!this->isSwapChainSupported(swap_chain_support_details)) {
        std::cerr << "[DEBUG] Swap chain is not supported" << std::endl;
        return VK_FALSE;
    }

    return VK_TRUE;
}

void
vulkanexamples::base::VulkanExampleBase::findQueueFamilies(const VkPhysicalDevice& physical_device) {
    this->_queue_family_indices = {};

    std::uint32_t queue_family_count {0};
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, nullptr);

    std::vector<VkQueueFamilyProperties> queue_families {queue_family_count};
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_count, queue_families.data());

    std::uint32_t flags = VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT;

    std::uint32_t i = 0;
    for (; i < queue_family_count; ++i) {
        VkQueueFamilyProperties& queue_family = queue_families[i];

        if ((queue_family.queueFlags & flags) == flags) {
            this->_queue_family_indices.graphics_queue_family = i;
            break;
        }
    }

    if (i == queue_family_count) {
        return;
    }

    VkBool32 present_support {false};
    vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, this->_surface, &present_support);

    if (present_support) {
        this->_queue_family_indices.present_queue_family = i;
    }
}

VkResult
vulkanexamples::base::VulkanExampleBase::queryDeviceExtensions(const VkPhysicalDevice& physical_device, std::vector<VkExtensionProperties>& device_extensions) {
    std::uint32_t device_extension_count {0};
    VK_CHECK(vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &device_extension_count, nullptr));

    device_extensions.resize(device_extension_count);
    VK_CHECK(vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &device_extension_count, device_extensions.data()));

    return VK_SUCCESS;
}

bool
vulkanexamples::base::VulkanExampleBase::checkDeviceExtensionSupport(std::vector<VkExtensionProperties>& device_extensions) {
    std::set<std::string_view> required_extensions {
        this->_enabled_device_extensions.begin(),
        this->_enabled_device_extensions.end()
        };

    for (const VkExtensionProperties& extension : device_extensions) {
        required_extensions.erase(extension.extensionName);
    }

    return required_extensions.empty();
}

VkResult
vulkanexamples::base::VulkanExampleBase::querySwapChainDetails(const VkPhysicalDevice& physical_device, SwapChainSupportDetails& swap_chain_support_details) {
    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, this->_surface, &swap_chain_support_details.capabilities));

    std::uint32_t format_count {0};
    VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, this->_surface, &format_count, nullptr));;

    if (format_count != 0) {
        swap_chain_support_details.formats.resize(format_count);
        VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, this->_surface, &format_count, swap_chain_support_details.formats.data()));
    }

    std::uint32_t present_modes_count;
    VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, this->_surface, &present_modes_count, nullptr));

    if (present_modes_count != 0) {
        swap_chain_support_details.present_modes.resize(present_modes_count);
        VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, this->_surface, &present_modes_count, swap_chain_support_details.present_modes.data()));
    }

    return VK_SUCCESS;
}

bool
vulkanexamples::base::VulkanExampleBase::isSwapChainSupported(const SwapChainSupportDetails& swap_chain_support_details) {
    return !(swap_chain_support_details.formats.empty() || swap_chain_support_details.present_modes.empty());
}

VkResult
vulkanexamples::base::VulkanExampleBase::createLogicalDevice() {
    float queue_priority {1.0f};

    std::set<std::uint32_t> unique_queue_families {
        this->_queue_family_indices.graphics_queue_family.value(),
        this->_queue_family_indices.present_queue_family.value(),
        };

    std::vector<VkDeviceQueueCreateInfo> device_queue_create_infos {};

    for (std::uint32_t queue_family : unique_queue_families) {
        VkDeviceQueueCreateInfo device_queue_create_info {
            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            nullptr,
            VkDeviceQueueCreateFlags{},
            queue_family,
            1,
            &queue_priority
            };

        device_queue_create_infos.emplace_back(device_queue_create_info);
    }

    VkPhysicalDeviceFeatures enabled_physical_device_features {};

    VkDeviceCreateInfo device_create_info {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        nullptr,
        VkDeviceCreateFlags{},
        static_cast<std::uint32_t>(device_queue_create_infos.size()),
        device_queue_create_infos.data(),
        static_cast<std::uint32_t>(this->_validation_layers.size()),
        this->_validation_layers.data(),
        static_cast<uint32_t>(this->_enabled_device_extensions.size()),
        this->_enabled_device_extensions.data(),
        &enabled_physical_device_features
        };

    VK_CHECK(vkCreateDevice(this->_physical_device, &device_create_info, nullptr, &this->_device));

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroyLogicalDevice() {
    vkDestroyDevice(this->_device, nullptr);
    this->_device = VK_NULL_HANDLE;
}

void
vulkanexamples::base::VulkanExampleBase::getGraphicsQueue() {
    vkGetDeviceQueue(this->_device, this->_queue_family_indices.graphics_queue_family.value(), 0, &this->_graphics_queue);
}

void
vulkanexamples::base::VulkanExampleBase::getPresentQueue() {
    vkGetDeviceQueue(this->_device, this->_queue_family_indices.present_queue_family.value(), 0, &this->_present_queue);
}

VkSurfaceFormatKHR
vulkanexamples::base::VulkanExampleBase::chooseSwapChainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats) {
    for (const VkSurfaceFormatKHR& format : formats) {
        if (format.format == VK_FORMAT_B8G8R8A8_SRGB
            && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return format;
        }
    }

    return formats.at(0);
}

VkPresentModeKHR
vulkanexamples::base::VulkanExampleBase::chooseSwapChainPresentMode(const std::vector<VkPresentModeKHR>& present_modes) {
    for (const VkPresentModeKHR& present_mode : present_modes) {
        if (present_mode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
            return present_mode;
        }
    }

    return present_modes.at(0);
}

VkExtent2D
vulkanexamples::base::VulkanExampleBase::chooseSwapChainExtent(const VkSurfaceCapabilitiesKHR& surface_capabilities) {
    if (surface_capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        return surface_capabilities.currentExtent;
    }

    int width {}, height {};
    SDL_Vulkan_GetDrawableSize(this->_window, &width, &height);

    VkExtent2D actual_extent {
        static_cast<std::uint32_t>(width),
        static_cast<std::uint32_t>(height)
        };

    actual_extent.width = std::clamp(
        actual_extent.width,
        surface_capabilities.minImageExtent.width,
        surface_capabilities.maxImageExtent.width
    );

    actual_extent.height = std::clamp(
        actual_extent.height,
        surface_capabilities.minImageExtent.height,
        surface_capabilities.maxImageExtent.height
    );

    return actual_extent;
}

VkResult
vulkanexamples::base::VulkanExampleBase::createSwapChain() {
    SwapChainSupportDetails swap_chain_support_details {};

    VK_CHECK(this->querySwapChainDetails(this->_physical_device, swap_chain_support_details));

    this->_swap_chain_details.capabilities = swap_chain_support_details.capabilities;

    this->_swap_chain_details.format = this->chooseSwapChainSurfaceFormat(swap_chain_support_details.formats);

    this->_swap_chain_details.present_mode = this->chooseSwapChainPresentMode(swap_chain_support_details.present_modes);

    this->_swap_chain_details.extent = this->chooseSwapChainExtent(swap_chain_support_details.capabilities);

    this->_swap_chain_details.image_count = this->_swap_chain_details.capabilities.minImageCount + 1;
    if (this->_swap_chain_details.capabilities.maxImageCount > 0 && this->_swap_chain_details.image_count > this->_swap_chain_details.capabilities.maxImageCount) {
        this->_swap_chain_details.image_count = this->_swap_chain_details.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR swap_chain_create_info {};
    swap_chain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swap_chain_create_info.surface = this->_surface;
    swap_chain_create_info.minImageCount = this->_swap_chain_details.image_count;
    swap_chain_create_info.imageFormat = this->_swap_chain_details.format.format;
    swap_chain_create_info.imageColorSpace = this->_swap_chain_details.format.colorSpace;
    swap_chain_create_info.imageExtent = this->_swap_chain_details.extent;
    swap_chain_create_info.imageArrayLayers = VULKAN_SWAP_CHAIN_IMAGE_ARRAY_LAYERS_2D;
    swap_chain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    std::array queue_families {
        this->_queue_family_indices.graphics_queue_family.value(),
        this->_queue_family_indices.present_queue_family.value(),
        };

    if (queue_families[0] != queue_families[1]) {
        swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swap_chain_create_info.queueFamilyIndexCount = queue_families.size();
        swap_chain_create_info.pQueueFamilyIndices = queue_families.data();
    } else {
        swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swap_chain_create_info.queueFamilyIndexCount = 0; // Optional
        swap_chain_create_info.pQueueFamilyIndices = nullptr; // Optional
    }

    swap_chain_create_info.preTransform =
        this->_swap_chain_details.capabilities.currentTransform;

    swap_chain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swap_chain_create_info.presentMode = this->_swap_chain_details.present_mode;
    swap_chain_create_info.clipped = VK_TRUE;


    VkSwapchainKHR old_swap_chain {this->_swap_chain ? this->_swap_chain : VK_NULL_HANDLE};
    swap_chain_create_info.oldSwapchain = old_swap_chain;

    VK_CHECK(vkCreateSwapchainKHR(this->_device, &swap_chain_create_info, nullptr, &this->_swap_chain));

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroySwapChain() {
    vkDestroySwapchainKHR(this->_device, this->_swap_chain, nullptr);
    this->_swap_chain = VK_NULL_HANDLE;
}

VkResult
vulkanexamples::base::VulkanExampleBase::createSwapChainImages() {
    std::uint32_t swap_chain_image_count {0};

    VK_CHECK(vkGetSwapchainImagesKHR(this->_device, this->_swap_chain, &swap_chain_image_count, nullptr));

    this->_swap_chain_images.resize(swap_chain_image_count);
    VK_CHECK(vkGetSwapchainImagesKHR(this->_device, this->_swap_chain, &swap_chain_image_count, this->_swap_chain_images.data()));

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroySwapChainImages() {
    // Do not destroy images since platform will destroy the swap
    // chain images automatically.

    this->_swap_chain_images.clear();
}


VkResult
vulkanexamples::base::VulkanExampleBase::createSwapChainImageViews() {
    this->_swap_chain_image_views.resize(this->_swap_chain_images.size());

    for (size_t i = 0; i < this->_swap_chain_images.size(); i++) {
        VkImageViewCreateInfo image_view_create_info {};
        image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        image_view_create_info.image = this->_swap_chain_images[i];

        image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        image_view_create_info.format = this->_swap_chain_details.format.format;

        image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

        image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_view_create_info.subresourceRange.baseMipLevel = 0;
        image_view_create_info.subresourceRange.levelCount = 1;
        image_view_create_info.subresourceRange.baseArrayLayer = 0;
        image_view_create_info.subresourceRange.layerCount = 1;


        VK_CHECK(vkCreateImageView(this->_device, &image_view_create_info, nullptr, &this->_swap_chain_image_views[i]));
    }

    return VK_SUCCESS;
}

void
vulkanexamples::base::VulkanExampleBase::destroySwapChainImageViews() {
    for (VkImageView& image_view : this->_swap_chain_image_views) {
        vkDestroyImageView(this->_device, image_view, nullptr);
    }

    this->_swap_chain_image_views.clear();
}

VkResult
vulkanexamples::base::VulkanExampleBase::recreateSwapChain() {
    this->destroySwapChainImageViews();
    this->destroySwapChainImages();
    this->destroySwapChain();

    VK_CHECK(this->createSwapChain());
    VK_CHECK(this->createSwapChainImages());
    VK_CHECK(this->createSwapChainImageViews());

    return VK_SUCCESS;
}

VkResult
vulkanexamples::base::VulkanExampleBase::createShaderModule(const std::vector<std::byte>& code, VkShaderModule& shader_module) {
    VkShaderModuleCreateInfo shader_module_create_info {};

    shader_module_create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shader_module_create_info.codeSize = code.size();
    shader_module_create_info.pCode = reinterpret_cast<const uint32_t*>(code.data());

    VK_CHECK(vkCreateShaderModule(this->_device, &shader_module_create_info, nullptr, &shader_module));

    return VK_SUCCESS;
}
