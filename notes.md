
## Queue Families
 - https://stackoverflow.com/a/55273688

## Pipeline
- https://vkguide.dev/docs/chapter-2/pipeline_walkthrough/

## Render Passes
 - https://developer.samsung.com/galaxy-gamedev/resources/articles/renderpasses.html
 - https://www.reddit.com/r/vulkan/comments/noi5dg/comment/h01lpao/?utm_source=share&utm_medium=web2x&context=3
 - https://www.youtube.com/watch?v=yeKxsmlvvus&list=PLMLurvdlOpWN62TnHlh9Ckb16jhlago0P&index=11&t=335s

## Subpasses
- https://stackoverflow.com/questions/76151028/what-is-a-subpass-and-how-does-it-relate-to-swapchains-and-framebuffers
- https://developer.samsung.com/galaxy-gamedev/resources/articles/renderpasses.html